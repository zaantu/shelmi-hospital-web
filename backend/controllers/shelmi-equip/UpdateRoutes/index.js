module.exports.set = function(app) {

    var jwt = require('jsonwebtoken');
    var secretkey = require('../../secretkey');


    
    var UsersModel = require('../../models/users');
    var TaskModel =require('../../models/tasks');
    var FinishedTaskModel = require('../../models/finishedTasks');
    var AccessoryModel = require('../../models/accessories');
    var SparesModel = require('../../models/spares');
    var EquipmentModel = require('../../models/equipment');
    var OrganisationModel = require('../../models/organisation');

//////////// FINISH SINGLE TASKS BELOW ///////////////////////// 
app.put('/api/web/finishTask/:id', function(req,res){

   
    var ID = req.params.id;

    var token = req.headers.authorization;
    
    

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');

        var chosenToken = splitJWTToken[0];
        var organID = splitJWTToken[1];

   

          // verify a token symmetric
jwt.verify(chosenToken, secretkey, function(err, decoded) {
    if(err) {



        res.json({
            status: false
        });

    } else if (decoded) {




        TaskModel.findByIdAndDelete({_id:ID}, (err,suc)=>{
            if(err){
                res.json({
                    deleting : false
                });
            } else if (suc) {

           
               
                var taskModel = new FinishedTaskModel();

                taskModel.taskType = req.body.taskType;
                taskModel.desc = req.body.description;
                taskModel.priority = req.body.priority;
                taskModel.due = req.body.due;
                taskModel.date = req.body.assignedDate;
                taskModel.inventoryNumber = req.body.inventoryNumber;
                taskModel.engravedNumber = req.body.engravedNumber;
                taskModel.engineer = req.body.assignedToId;
                taskModel.assignedBy = req.body.assignedById;
        
                taskModel.save((err, saved)=>{
                    if (err){

                        console.log(err)
        
                    } else if (saved){

                        

                        var finishedTaskID = saved._id;

                        var removeAdd ={
                            $pull: {'pendingTasks': req.params.id},
                            $push: {'finishedTasks': finishedTaskID}
                        }

                        OrganisationModel.findByIdAndUpdate({_id: organID},
                            removeAdd, {new:true, upsert: true},
                            (err,success) => {
                                if (err) {

                                    console.log(err)

                                } else if (success) {

                                    

                                // finding the specific equipment below

                                if (req.body.inventoryNumber !== '' && req.body.engravedNumber === '') {


                                    // searching by inventory number
                                    OrganisationModel.findById({_id: organID})
                                    .populate({path: 'equipment', match: { 'inventoryNumber' : req.body.inventoryNumber}})
                                    .exec((err,found)=>{
                                        if (err) {

                                            console.log(err)

                                            res.json({
                                              finished: false
                                            })
                                        } 
                                        
                                        
                                        else if (found.equipment.length > 0) {


                                            var equipmentID = found.equipment[0]._id;

                                            var updatingEquipment ={
                                                $pull: {'tasks': req.params.id},
                                                $push: {'finishedTasks': finishedTaskID}
                                            }

                                            EquipmentModel.findByIdAndUpdate({_id: equipmentID},
                                                updatingEquipment, {upsert:true, new:true}, (err,updated)=> {
                                                    if (err){



                                                    } else if (updated){

                                                        res.json({
                                                            finished: true,
                                        
                                                        });

                                                    }
                                                })

                                        }

                                    });

                                } else if (req.body.inventoryNumber === '' && req.body.engravedNumber !== '') {


                                    // searching by engraved number
                                    OrganisationModel.findById({_id: organID})
                                    .populate({path: 'equipment', match: { 'engravedNumber' : req.body.engravedNumber}})
                                    .exec((err,found)=>{
                                        if (err) {

                                            console.log(err)

                                            res.json({
                                              finished: false
                                            })
                                        } 
                                        
                                        
                                        else if (found.equipment.length > 0) {


                                            var equipmentID = found.equipment[0]._id;

                                            var updatingEquipment ={
                                                $pull: {'tasks': req.params.id},
                                                $push: {'finishedTasks': finishedTaskID}
                                            }

                                            EquipmentModel.findByIdAndUpdate({_id: equipmentID},
                                                updatingEquipment, {upsert:true, new:true}, (err,updated)=> {
                                                    if (err){



                                                    } else if (updated){

                                                        res.json({
                                                            finished: true,
                                        
                                                        });

                                                    }
                                                })

                                        }

                                    });

                                } else if (req.body.inventoryNumber === '' && req.body.engravedNumber === '') {

                                    res.json({
                                        finished: true,
                    
                                    });

                                } else if (!req.body.inventoryNumber && !req.body.engravedNumber) {

                                    res.json({
                                        finished: true,
                    
                                    });
                                }

                                   
                                }
                            });


                       
        
                    }
                });

                
            }
        }); 
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

//////// UPDATE SINGLE TASK BELOW /////////
app.put('/api/web/updateTask/:id', function(req,res){

    var ID = req.params.id;

    var token = req.headers.authorization;
    

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();


          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {



        res.json({
            status: false
        });

    } else if (decoded) {
        var taskModel = new FinishedTaskModel();

        // update code
        var updating = {
            'taskType': req.body.taskType,
                'desc': req.body.description,
                'priority': req.body.priority,
                'due': req.body.due,
                'date': req.body.assignedDate,
                'engineer': req.body.assignedToId,
                'assignedBy':  req.body.assignedById

        };


        TaskModel.findByIdAndUpdate({_id:ID},updating, {upsert: true, new: true})
        .populate('engineer assignedBy').exec( (err,suc)=>{
            if(err){
                res.json({
                    updated : false
                });
            } else if (suc) {
                res.json({
                    updated : true,
                    data: suc
                });
              
            }
        }); 
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

});

/// update Single Accessory Item below
app.put('/api/web/updateAccessoryItem/:id', function(req,res){

    var ID = req.params.id;

    var token = req.headers.authorization;
    

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();


          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {



        res.json({
            status: false
        });

    } else if (decoded) {

        // update code
        var updating = {
            'name': req.body.name,
                'desc': req.body.desc,
                'serialNumber': req.body.serialNumber,
                'modelNumber': req.body.modelNumber,
                'quantity': req.body.qnty,

        };


        AccessoryModel.findByIdAndUpdate({_id:ID},updating, {upsert: true, new: true}, (err,suc)=>{
            if(err){
                res.json({
                    updated : false
                });
            } else if (suc) {

                res.json({
                    updated : true,
                    data: suc
                });
              
            }
        }); 
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

});

// update spare part
app.put('/api/web/updateSpareItem/:id', function(req,res){

    var ID = req.params.id;

    var token = req.headers.authorization;
    

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();


          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {



        res.json({
            status: false
        });

    } else if (decoded) {

        // update code
        var updating = {
            'name': req.body.name,
                'desc': req.body.desc,
                'serialNumber': req.body.serialNumber,
                'modelNumber': req.body.modelNumber,
                'quantity': req.body.qnty,

        };


        SparesModel.findByIdAndUpdate({_id:ID},updating, {upsert: true, new: true}, (err,suc)=>{
            if(err){
                res.json({
                    updated : false
                });
            } else if (suc) {

                res.json({
                    updated : true,
                    data: suc
                });
              
            }
        }); 
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

});

// update equipment basic information below
app.put('/api/web/updateEquipmentItem/:id', (req,res) => {

    var ID = req.params.id;

    var token = req.headers.authorization;

  if(token) {

    var jwtToken = token.slice(6, token.length).trimLeft();


  // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {

        if(!req.body.contactPersonPhone) {

            var updating = {

                'equipmentName': req.body.equipmentName,
                'equipmentModel': req.body.model,
                'engravedNumber': req.body.engravedNumber,
                'equipmentType': req.body.equipmentType,
                'serialNumber':  req.body.serialNumber,
                'desc': req.body.equipmentDescription,
                'physicalLocation':  req.body.physicalLocation,
                'departmentLocation': req.body.departmentLocation,
                'departmentContactPerson': req.body.departmentConctactPerson,
                'contactPersonCode': '',
                'contactPersonNumber':  '',
                'contactPersonCountryCode': '',
                'serviceRequirements': req.body.serviceRequirement,
                'maintenanceRequirements': req.body.maintenanceRequirement,
                'equipmentFunction': req.body.equipmentFunction,
                'physicalRisk': req.body.physicalRisk,
                'sparesAvailable': req.body.sparesAvailable,
                'willBeMoved': req.body.willMove,
                'equipmentCondition': req.body.equipmentCondition,
                'powerRequirement': req.body.powerRequirement,
                'currencyCode': req.body.currencyCode,
                'cost': req.body.purchaseCost,
                'otherNotes': req.body.otherNotes,
                'comissionDate': req.body.commissionDate,
                $set: { equipmentUsers: [] },
            }
       

            EquipmentModel.findByIdAndUpdate({_id: ID}, updating, {new:true}, function(err,updated) {
                if(err){

                } else if (updated) {
                    var  updatingArray = {
                        $push: {equipmentUsers: req.body.equipmentUsers}
    
                      }
                        
    
                      EquipmentModel.findByIdAndUpdate({_id: ID}, updatingArray, {new:true}, function(err,updated) {
    
                        if(err) {
    
                        } else {
                            res.json({
                                data: updated,
                                updating: true
            
                            });
    
                        }
                      });
                }
            })



        } else if (req.body.contactPersonPhone) {


            var updating = {

                'equipmentName': req.body.equipmentName,
                'equipmentModel': req.body.model,
                'engravedNumber': req.body.engravedNumber,
                'equipmentType': req.body.equipmentType,
                'serialNumber':  req.body.serialNumber,
                'desc': req.body.equipmentDescription,
                'physicalLocation':  req.body.physicalLocation,
                'departmentLocation': req.body.departmentLocation,
                'departmentContactPerson': req.body.departmentConctactPerson,
                'contactPersonCode':  req.body.contactPersonPhone.dialCode,
                'contactPersonCountryCode': req.body.contactPersonPhone.countryCode,
                'contactPersonNumber': req.body.contactPersonPhone.nationalNumber,
                'serviceRequirements': req.body.serviceRequirement,
                'maintenanceRequirements': req.body.maintenanceRequirement,
                'equipmentFunction': req.body.equipmentFunction,
                'physicalRisk': req.body.physicalRisk,
                'sparesAvailable': req.body.sparesAvailable,
                'willBeMoved': req.body.willMove,
                'equipmentCondition': req.body.equipmentCondition,
                'powerRequirement': req.body.powerRequirement,
                'currencyCode': req.body.currencyCode,
                'cost': req.body.purchaseCost,
                'otherNotes': req.body.otherNotes,
                'comissionDate': req.body.commissionDate,
                $set: { equipmentUsers: [] },

            }


            EquipmentModel.findByIdAndUpdate({_id: ID}, updating, {new:true}, function(err,updated) {
                if(err){

                } else if (updated) {

                  var  updatingArray = {
                    $push: {equipmentUsers: req.body.equipmentUsers}

                  }
                    

                  EquipmentModel.findByIdAndUpdate({_id: ID}, updatingArray, {new:true}, function(err,updated) {

                    if(err) {

                    } else {
                        res.json({
                            data: updated,
                            updating: true
        
                        });

                    }
                  });
                          
    
                }
            })

        }
  
    }
    
  });

  } else if(!token) {

    res.json({
        status: false
    });
  }
})

// update manufacturer details below
app.put('/api/web/updateEquipmentSingleManufacturer/:id', function(req,res){

    var ID = req.params.id;

    var token = req.headers.authorization;

    if(token) {
  
      var jwtToken = token.slice(6, token.length).trimLeft();
  
  
    // verify a token symmetric
  jwt.verify(jwtToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else if(decoded) {  
  

                  updating = {
  
                      'manufacturer.name': req.body.manuName,
                      'manufacturer.businessCode': req.body.manuBssCode,
                      'manufacturer.email': req.body.manuEmail,
                      'manufacturer.countryCode': req.body.countryCode,
                      'manufacturer.businessNumber': req.body.manuBssNumber,
                      'manufacturer.fax': req.body.manuFax,
                      'manufacturer.street': req.body.manuStreet,
                      'manufacturer.city': req.body.manuCity,
                      'manufacturer.state': req.body.manuStreet,
                      'manufacturer.country': req.body.manuCountry,
                      'manufacturer.zipPostal': req.body.manuZip,
                      'manufacturer.otherNotes': req.body.manuOtherNotes
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: ID}, updating, {upsert: true}, (err,success) => {
                      if(err){

  
  
                      } else if (success) {
  
                          res.json({
                              save: true
                          });
                      }
                  });
    
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }
})

// updated vendor details below
app.put('/api/web/updateVendor/:id', function(req,res) {

    var ID = req.params.id;

    var token = req.headers.authorization;

    if(token) {
  
      var jwtToken = token.slice(6, token.length).trimLeft();
  
  
    // verify a token symmetric
  jwt.verify(jwtToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else if(decoded) {  
  

                  updating = {
  
                    'vendor.fName': req.body.vendorFirstName,
                    'vendor.surname':  req.body.vendorSurname,
                    'vendor.email':  req.body.vendorEmail,
                    'vendor.company': req.body.vendorCompany,
                    'vendor.BssCode': req.body.vendorBssCode,
                    'vendor.BssCountryCode': req.body.BssCountryCode,
                    'vendor.BssNumber': req.body.vendorBssNumber,
                    'vendor.mobileCountryCode': req.body.mobileCountryCode,
                    'vendor.mobileCode':  req.body.vendorMobileCode,
                    'vendor.mobileNumber':  req.body.vendorMobileNumber,
                    'vendor.fax':  req.body.vendorFax,
                    'vendor.street':  req.body.vendorStreet,
                    'vendor.city': req.body.vendorCity,
                    'vendor.state': req.body.vendorState,
                    'vendor.country':  req.body.vendorCountry,
                    'vendor.zipPostal': req.body.vendorZipPostal,
                    'vendor.otherNotes': req.body.vendorOtherNotes,
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: ID}, updating, {upsert: true}, (err,success) => {
                      if(err){

  
                      } else if (success) {
  
                          res.json({
                              updating: true
                          });
                      }
                  });
    
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }
})

// update service vendor details below
app.put('/api/web/updateServiceVendor/:id', function(req,res) {

    var ID = req.params.id;

    var token = req.headers.authorization;

    if(token) {
  
      var jwtToken = token.slice(6, token.length).trimLeft();
  
  
    // verify a token symmetric
  jwt.verify(jwtToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else if(decoded) {  
  

                  updating = {
  
                    'serviceVendor.fName': req.body.vendorFirstName,
                    'serviceVendor.surname':  req.body.vendorSurname,
                    'serviceVendor.email':  req.body.vendorEmail,
                    'serviceVendor.company': req.body.vendorCompany,
                    'serviceVendor.BssCode': req.body.vendorBssCode,
                    'serviceVendor.BssCountryCode': req.body.BssCountryCode,
                    'serviceVendor.BssNumber': req.body.vendorBssNumber,
                    'serviceVendor.mobileCountryCode': req.body.mobileCountryCode,
                    'serviceVendor.mobileCode':  req.body.vendorMobileCode,
                    'serviceVendor.mobileNumber':  req.body.vendorMobileNumber,
                    'serviceVendor.fax':  req.body.vendorFax,
                    'serviceVendor.street':  req.body.vendorStreet,
                    'serviceVendor.city': req.body.vendorCity,
                    'serviceVendor.state': req.body.vendorState,
                    'serviceVendor.country':  req.body.vendorCountry,
                    'serviceVendor.zipPostal': req.body.vendorZipPostal,
                    'serviceVendor.otherNotes': req.body.vendorOtherNotes,
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: ID}, updating, {upsert: true}, (err,success) => {
                      if(err){

  
                      } else if (success) {
  
                          res.json({
                              updating: true
                          });
                      }
                  });
    
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }
})

};