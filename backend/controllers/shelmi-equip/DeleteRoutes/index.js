module.exports.set = function(app) {

    var jwt = require('jsonwebtoken');
    var secretkey = require('../../secretkey');
    var fs = require('fs');


    
    var UsersModel = require('../../models/users');
    var AccessoryModel = require('../../models/accessories');
    var TaskModel =require('../../models/tasks');
    var EquipmentModel = require('../../models/equipment');
    var SparesModel = require('../../models/spares');
    var OrganisationModel = require('../../models/organisation');
    var PMSModel = require('../../models/PMS');
    var ManualsModel = require('../../models/manuals');
    var PendingUserModel = require('../../models/temporaryusers');

    /////////////////// DELETE USER BELOW ///////////////////////////

    app.delete('/api/web/deleteUser/:id', function(req,res) {

        var ID = req.params.id;

        var token = req.headers.authorization;

        if(token) {
            var jwtToken = token.slice(6, token.length).trimLeft();
    
            var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
    
    
              // verify a token symmetric
    jwt.verify(chosenToken, secretkey, function(err, decoded) {
        if(err) {
    
            res.json({
                status: false
            });
    
        } else if (decoded) {
    
        
    
            UsersModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
                if(err){
                    res.json({
                        deleting : false
                    });
                } else if (suc) {
    
                    var removing = {
                        $pull: {'users': req.params.id}
                    }
            
                    OrganisationModel.findByIdAndUpdate({_id: organID}, removing,
                        {upsert: true, new: true}, (err,updated) =>{
                            if(err) {
    
                                console.log(err)
            
                            } else if (updated) {
            
                                res.json({
                                    deleting : true
                                });
                            }
                        } );
                }
            });
        }
    
    
    
        }); 
         
        }
            else if(!token) {
            res.json({
                status: false
            });
    
        }


    })

    /////////////////////////////////////////////////////////////////

    ///////////////////////// DELETE PENDING USER ////////////////
    app.delete('/api/web/deletePendingUser/:id', function(req,res) {
        var ID = req.params.id;

        var token = req.headers.authorization;

        if(token) {
            var jwtToken = token.slice(6, token.length).trimLeft();
    
            var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
    
    
              // verify a token symmetric
    jwt.verify(chosenToken, secretkey, function(err, decoded) {
        if(err) {
    
            res.json({
                status: false
            });
    
        } else if (decoded) {
    
        
    
            PendingUserModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
                if(err){
                    res.json({
                        deleting : false
                    });
                } else if (suc) {
    
                    var removing = {
                        $pull: {'temporaryUsers': req.params.id}
                    }
            
                    OrganisationModel.findByIdAndUpdate({_id: organID}, removing,
                        {upsert: true, new: true}, (err,updated) =>{
                            if(err) {
    
                                console.log(err)
            
                            } else if (updated) {
            
                                res.json({
                                    deleting : true
                                });
                            }
                        } );
                }
            });
        }
    
    
    
        }); 
         
        }
            else if(!token) {
            res.json({
                status: false
            });
    
        }

    })


    //////////////////////////////////////////////////////////////

//////////// DELETE SINGLE TASKS BELOW ///////////////////////// 
app.delete('/api/web/deleteTask/:id', function(req,res){

    var ID = req.params.id;

    var token = req.headers.authorization;

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');

        var chosenToken = splitJWTToken[0];
        var organID = splitJWTToken[1];


          // verify a token symmetric
jwt.verify(chosenToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {

    

        TaskModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
            if(err){
                res.json({
                    deleting : false
                });
            } else if (suc) {

                var removing = {
                    $pull: {'pendingTasks': req.params.id}
                }
        
                OrganisationModel.findByIdAndUpdate({_id: organID}, removing,
                    {upsert: true, new: true}, (err,updated) =>{
                        if(err) {

                            console.log(err)
        
                        } else if (updated) {
        
                            res.json({
                                deleting : true
                            });
                        }
                    } );
            }
        });
    }



    }); 
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

//  Delete single equipment below
app.delete('/api/web/deleteEquipment/:id',  function(req,res){

    var ID = req.params.id;

    var uncleanToken  = req.headers.authorization;

    var cleaningToken = uncleanToken.split(/\s+/);

    var token = cleaningToken[0];
    var institutionID = cleaningToken[1];

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {


        EquipmentModel.findOne({_id: ID}, (err, found) => {
            if (err) {

            } else if (found){

//////////// deleting the accesories from model ////////////////////

if (found.accessories.length === 0) {

    // do nothing on the next one
} else {

    AccessoryModel.deleteMany({_id : {$in : found.accessories}}, (err,result) => {
        if (err) {

        } else if (result) {

            // successfull deleted from model
        }
    })
}
          
////////////////////////////////////////////////////////////////

////////////// deleting the spares from model ///////////////////
if (found.spares.length === 0) {

    // do nothing
} else {
    SparesModel.deleteMany({_id: {$in: found.spares}}, (err, result) => {
        if (err) {

        } else if (result) {
// successfully deleted from model
        }
    })
}


///////////////////////////////////////////////////////////////


/////////////// deleting from pms model ////////////////////
if (found.PMS.length === 0) {
    /// do nothing
} else {
    PMSModel.deleteMany({_id: {$in: found.PMS}}, (err, result) => {

        if (err) {

        } else if (result) {
            // success
        }
    })

}
////////////////////////////////////////////////////////////

//////////// deleting from Manuals Model ////////////////////////////
if (found.manuals.length === 0) {

} else {

    var newArray = [];
    newArray = found.manuals;

    ManualsModel.deleteMany({_id: {$in : found.manuals}}, (err, result) => {
        if (err){

        } else if (result) {

            // delete the pdf files that already exist on the system

            for (var i = 0; i < newArray.length; i++) {
               
  fs.unlink( './../../_MANUALS_FOLDER/' + newArray[i] + '.pdf', function (err) {


   if (err) throw err;
    // if no error, file has been deleted successfully
 
}); 
            }
        }
    })
}

//////////////////////////////////////////////////////////////////////



////////////// update id from the organisation //////////////////////////

removeID = {
    $pull: {equipment: ID}
}


    OrganisationModel.findByIdAndUpdate({'_id': institutionID}, removeID, {new:true, upsert:true}, (err,updated) => {
        if (err){

           // console.log(err)
        } else if (updated) {

            EquipmentModel.findOneAndDelete({_id: ID}, (err,deleted) => {
                if (err) {

                } else if(deleted) {
                    res.json({
                        deleting: true
                    })
                }
            })
        }
    }) 







/////////////////////////////////////////////////////////////

            }
        })

    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

// delete single accessory item below
app.delete('/api/web/deleteAccessoryItem/:id', function(req,res){

    var uncleanID = req.params.id;

  
    var cleaningID = uncleanID.split(/\s+/);

    var ID = cleaningID[0];

    var equipID = cleaningID[1];

    var token = req.headers.authorization;
    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {

    var updating = {
        $pull: {accessories: ID}
    }

    EquipmentModel.findByIdAndUpdate({_id: equipID}, updating, {new:true, upsert: true}, (err,updated) => {
        if (err) {

        } else if (updated) {

            AccessoryModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
                if(err){
                    res.json({
                        deleting : false
                    });
                } else if (suc) {
                    res.json({
                        deleting : true
                    });
                }
            });
        }
    })
    
     
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

// delete single spares item
app.delete('/api/web/deleteSparesItem/:id', function(req,res){

    var uncleanID = req.params.id;

  
    var cleaningID = uncleanID.split(/\s+/);

    var ID = cleaningID[0];

    var equipID = cleaningID[1];

    var token = req.headers.authorization;

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {

        var updating ={
        $pull: {spares: ID}
        }

        EquipmentModel.findByIdAndUpdate({_id: equipID}, updating, {new:true, upsert: true}, (err,updated) => {
            if (err) {

            } else if (updated) {

                SparesModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
                    if(err){
                        res.json({
                            deleting : false
                        });
                    } else if (suc) {
                        res.json({
                            deleting : true
                        });
                    }
                });

            }
        })
    
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

// delete single manual item below
app.delete('/api/web/deleteSingleManual/:id', function(req,res){

    var uncleanID = req.params.id;

  
    var cleaningID = uncleanID.split(/\s+/);

    var ID = cleaningID[0];

    var equipID = cleaningID[1];

    var token = req.headers.authorization;

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else if (decoded) {

        var updating ={
        $pull: {manuals: ID}
        }

        EquipmentModel.findByIdAndUpdate({_id: equipID}, updating, {new:true, upsert: true}, (err,updated) => {
            if (err) {

            } else if (updated) {

                ManualsModel.findByIdAndRemove({_id:ID}, (err,suc)=>{
                    if(err){
                        res.json({
                            deleting : false
                        });
                    } else if (suc) {

                        fs.unlink( './../../_MANUALS_FOLDER/' + ID + '.pdf', function (err) {


                            if (err) throw err;
                             // if no error, file has been deleted successfully

                             res.json({
                                deleting : true
                            });
                          
                         });

                        
                    }
                });

            }
        })
    
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

   
});

};