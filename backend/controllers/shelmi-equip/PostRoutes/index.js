module.exports.set = function(app) {

    var jwt = require('jsonwebtoken');
    var fs = require('fs-extra');
    var multer = require('multer');
    var moment = require('moment');
    var nodemailer = require('nodemailer');
    const request = require('request');



  var UserModel = require('../../models/users');
  var CountryModel = require('../../models/country');
  var OrganisationModel = require('../../models/organisation');
  var TaskModel = require('../../models/tasks');
  var EquipmentModel = require('../../models/equipment');
  var PMSModel = require('../../models/PMS');
  var ManualsModel = require('../../models/manuals');
  var AccessoriesModel = require('../../models/accessories');
  var SparesModel = require('../../models/spares');
  var TemporaryUserModel = require('../../models/temporaryusers');

  var secretkey = require('../../secretkey');

  /// save country below /////////
  app.post('/api/web/organisationSignup', (req,res)=>{

      CountryModel.findOne({country: req.body.country}, (err,found) => {
          if(err) {

          } else if (!found) { // if country name is not found

            var countryModel = new CountryModel();

            countryModel.country = req.body.country;
        
            countryModel.save((err,saved) => {
                if (err) {
        
                } else if (saved) {
                    var ID = saved._id;
        
        
                   OrganisationModel.findOne({name: req.body.organisation}, (err,found)=>{
                       if (err) {
        
        
                       } else if (!found) {
        
                        var organisationModel = new OrganisationModel();
                        organisationModel.name = req.body.organisation;
        
                        organisationModel.save((err,saved)=>{
                            if (err){
        
                            } else if (saved) {

                                var organisationID = saved._id;
                                var organisationName = saved.name;


                                var updating ={
                                    $push:
                                        {'organisation': organisationID}
                                }
        
                                CountryModel.findOneAndUpdate({country: req.body.country},
                                    updating, {new: true, upsert: true}, (err,saved) =>{
                                        if (err){
        
                                        } else if (saved) {
                                            res.json({
                                                success: true,
                                                organisationID: organisationID,
                                                organisationName: organisationName
                                            })
                                        }
                                    })
        
                            }
                        })
        
                       } else if (found) {
        
                        res.json({
                            success: true

                            
                        })
        
                       }
                   })
                }
            })

          } else if (found) { // if country name is found


            OrganisationModel.findOne({name: req.body.organisation}, (err,found)=>{
                if (err) {
 
 
                } else if (!found) {
 
                 var organisationModel = new OrganisationModel();
                 organisationModel.name = req.body.organisation;

 
                 organisationModel.save((err,saved)=>{
                     if (err){
 
                     } else if (saved) {

                        var organisationID = saved._id;
                        var organisationName = saved.name;



                        var updating ={
                            $push:
                                {'organisation': organisationID}
                        }

                        CountryModel.findOneAndUpdate({country: req.body.country},
                            updating, {new: true, upsert: true}, (err,saved) =>{
                                if (err){

                                } else if (saved) {
                                    res.json({
                                        success: true,
                                        organisationID: organisationID,
                                        organisationName: organisationName
                                    })
                                }
                            })
 
                     }
                 })
 
                } else if (found) {
 
                 res.json({

                    success: true
                     
                 })
 
                }
            })
          }
      })

 


  })

  // sending invite to email user

  app.post('/api/web/emailInvite', (req,res) => {

    UserModel.findOne({email: req.body.email}, (err,found) => {

        if (err) {

        } else if (found) {
            res.json({
                emailMatch: true
            })
        } else if (!found) {


    TemporaryUserModel.findOne({email: req.body.email}, (err,found) =>{

        if (err){

            console.log(err);

        } else if (found) {

            tempToken = found.token;

            jwt.verify(tempToken, secretkey, (err,decoded) => {
                if (err) {

                    var ID = found._id;

                    jwt.sign({ID}, secretkey, { expiresIn: '24h' },  (err,token) => {

                        if (err) {
                            console.log(err)
                        } else if (token) {

                            const url = req.protocol + '://' + 'equip.zaantu.com' + '/verify-token/' + ID;

                            

                            const options = {
                                method: 'POST',
                                url: 'https://api.sendinblue.com/v3/smtp/email',
                                headers: {
                                  accept: 'application/json',
                                  'content-type': 'application/json',
                                  'api-key': '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'
                                },
                                body: {
                                  sender: {name: 'Zaantu Administrator', email: 'admin@zaantu.com'},
                                  to: [{email: req.body.email}],
                                  htmlContent: '<div style="padding-top:20px; padding-left:10%;padding-right:10%; background-color:#f2f2f2;">' +
                                  '<div style="background-color:#fff; margin-bottom:2%;">' + 
                                  '<img style="text-align:center; display: block;   margin-left: auto;   margin-right: auto;"' +
                                   'src="https://i.ibb.co/5v10PHR/log.png" alt="Zaantu logo" width="150" height="100">' +
                                    '<h3 style="color:#ff9800; margin-left: 2%"> Shelmi Equip Software</h3> <b style="margin-left:2%;">' +
                                     'Hi, </b> <p style="margin-left:2%"> You are receiving this email to enable you verify your email address.' +
                                      'Click the link below in order to verify your email and gain access to the software;</p>' + 
                                      '<p style="margin-left:2%">' + url + '</p> </div>' +
                                      '<div style="text-align:center; display: block;   margin-left: auto;   margin-right: auto; ' +
                                       'margin-bottom:5%">' +
                                       '<a href="https://shelmi.zaantu.com/privacypolicy" target="_blank" style="text-decoration: none;' +
                                        'cursor: pointer; color:#000"> Privacy Policy </a>' + '| ' +
                                       '<a href="https://shelmi.zaantu.com/contact" target="_blank" style="text-decoration: none;' +
                                        'cursor: pointer; color:#000"> Contact us </a> <p> Mbogo Road II, Kampala, Uganda </p></div> </div>',
                                 subject: 'SHELMI EQUIP EMAIL VERIFICATION',
                                },
                                json: true
                              };
                              
                              request(options, function (error, response, body) {
                                if (error) throw new Error(error);
                              
                              });
        
                              addToken ={
                                  $set: {'token': token}
                              }
        
                              TemporaryUserModel.findByIdAndUpdate({_id:ID},addToken,{new:true, upsert:true}, (err,updated) => {
        
                                if(err) {
        
                                } else if (updated) {
                                  
                                    res.json({
        
                                        successExists: true
                                    })
                                }
        
                              })

                        }
                    })

                
                } else if (decoded) {


                    res.json({

                        emailExists: true

                    })

                  
                }
            })


        } else if (!found) {

           
// first create this email in the contact list of our send in blue server
const options = {
  method: 'POST',
  url: 'https://api.sendinblue.com/v3/contacts',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    'api-key': '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'
  },
  body: {updateEnabled: false, email: req.body.email},
  json: true
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

});


                    var temporaryUserModel = new TemporaryUserModel();

                    temporaryUserModel.email = req.body.email;
                    temporaryUserModel.role = 'Technician';
                    temporaryUserModel.organisations.push(req.body.organisationID);

                    temporaryUserModel.save((err,saved) => {
                        if (err) {

                            console.log(err)

                        } else if (saved) {

                            var ID = saved._id;

                        
                            var updating = {
                                $push:
                                {'temporaryUsers': ID}
                            }

                            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                updating, {upsert: true, new : true}, (err,updated)=>{
                                    if(err) {
                                         
                                    } else if (updated) {


                                     jwt.sign({ID}, secretkey, { expiresIn: '24h' }, (err,token)=>{
                                            if(err){
                                                res.json({
                                                    login:false
                                                });
                                            } else {
            

                                                const url = req.protocol + '://' + 'equip.zaantu.com' + '/verify-token/' + ID;

                                                const options = {
                                                    method: 'POST',
                                                    url: 'https://api.sendinblue.com/v3/smtp/email',
                                                    headers: {
                                                      accept: 'application/json',
                                                      'content-type': 'application/json',
                                                      'api-key': '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'
                                                    },
                                                    body: {
                                                      sender: {name: 'Zaantu Administrator', email: 'admin@zaantu.com'},
                                                      to: [{email: req.body.email}],
                                                      htmlContent: '<div style="padding-top:20px; padding-left:10%;padding-right:10%; background-color:#f2f2f2;">' +
                                                       '<div style="background-color:#fff; margin-bottom:2%;">' + 
                                                       '<img style="text-align:center; display: block;   margin-left: auto;   margin-right: auto;"' +
                                                        'src="https://i.ibb.co/5v10PHR/log.png" alt="Zaantu logo" width="150" height="100">' +
                                                         '<h3 style="color:#ff9800; margin-left: 2%"> Shelmi Equip Software</h3> <b style="margin-left:2%;">' +
                                                          'Hi, </b> <p style="margin-left:2%"> You are receiving this email to enable you verify your email address.' +
                                                           'Click the link below in order to verify your email and gain access to the software;</p>' + 
                                                           '<p style="margin-left:2%">' + url + '</p> </div>' +
                                                           '<div style="text-align:center; display: block;   margin-left: auto;   margin-right: auto; ' +
                                                            'margin-bottom:5%">' +
                                                            '<a href="https://shelmi.zaantu.com/privacypolicy" target="_blank" style="text-decoration: none;' +
                                                             'cursor: pointer; color:#000"> Privacy Policy </a>' + '| ' +
                                                            '<a href="https://shelmi.zaantu.com/contact" target="_blank" style="text-decoration: none;' +
                                                             'cursor: pointer; color:#000"> Contact us </a> <p> Mbogo Road II, Kampala, Uganda </p></div> </div>',
                                                      subject: 'SHELMI EQUIP EMAIL VERIFICATION',
                                                    },
                                                   
                                                    json: true
                                                  };
                                                  
                                                  request(options, function (error, response, body) {
                                                    if (error) throw new Error(error);

                                                    console.log(response);
                                                  
                                                  });

                                                  addToken ={
                                                      $set: {'token': token}
                                                  };
            
                                                  TemporaryUserModel.findByIdAndUpdate({_id:ID},addToken,{new:true, upsert:true}, (err,updated) => {

                                                    if(err) {

                                                    } else if (updated) {
                                                        res.json({
                                                            success: true,
                                                        
                                                        });
                                                    }

                                                  })
                                               
                                            }
                                        });

                                    }
                                })
                        }
                    })

                    
                }
            

        
    })

}
})

  })
  // signing up administrator below
app.post('/api/web/signup', (req,res)=>{

    UserModel.findOne({email: req.body.email}, (err,found) =>{

        if (err){

            console.log(err);

        } else if (found) {
            res.json({
                emailExists: true
            })
        } else if (!found) {

                    var userModel = new UserModel();

                    userModel.fname = req.body.fname;
                    userModel.surname = req.body.surname;
                    userModel.email = req.body.email;
                    userModel.role = 'Biomed Technician Lead';
                    userModel.password = req.body.password;
                    userModel.organisations.push(req.body.organisationID);

                    userModel.save((err,saved) => {
                        if (err) {

                            console.log(err)

                        } else if (saved) {

                            var ID = saved._id;

                        
                            var updating = {
                                $push:
                                {'users': ID}
                            }

                            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                updating, {upsert: true, new : true}, (err,updated)=>{
                                    if(err) {
                                         
                                    } else if (updated) {


                                        jwt.sign({ID}, secretkey, { expiresIn: '12h' }, (err,token)=>{
                                            if(err){
                                                res.json({
                                                    login:false
                                                });
                                            } else {
            
                                               // res.cookie('jwt',token, { httpOnly: false, secure: false, path: '/', maxAge: 720000000 });
            
                                                res.json({
                                                    signup: true,
                                                    token: token
                                                    
                    
                                                });
                                            }
                                        });

                                    }
                                })
                        }
                    })

                    
                
            

        }
    })
})

// verifying email here
app.post('/api/web/confirm-verification', (req,res) => {

    TemporaryUserModel.findByIdAndRemove({_id: req.body.temporaryID}, (err, suc) => {

        if (err) {

        } else if (suc) {

            var removeUpdating = {
                $pull:
                {'temporaryUsers': req.body.temporaryID}
            }

            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                removeUpdating, {upsert: true, new : true}, (err,updated)=>{
                    if(err) {
                         
                    } else if (updated) {

                            if(err){
                                res.json({
                                    login:false
                                });
                            } else {

                                var userModel = new UserModel();

                                userModel.fname = req.body.fname;
                                userModel.surname = req.body.surname;
                                userModel.email = req.body.email;
                                userModel.role = 'Biomed Technician';
                                userModel.password = req.body.password;
                                userModel.organisations.push(req.body.organisationID);
            
                                userModel.save((err,saved) => {
                                    if (err) {
            
                                    } else if (saved) {
            
                                        var ID = saved._id;
            
                                    
                                        var updating = {
                                            $push:
                                            {'users': ID}
                                        }
            
                                        OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                            updating, {upsert: true, new : true}, (err,updated)=>{
                                                if(err) {
                                                     
                                                } else if (updated) {
            
            
                                                    jwt.sign({ID}, secretkey, { expiresIn: '12h' }, (err,token)=>{
                                                        if(err){
                                                            res.json({
                                                                login:false
                                                            });
                                                        } else {
                        
                                                           // res.cookie('jwt',token, { httpOnly: false, secure: false, path: '/', maxAge: 720000000 });
                        
                                                            res.json({
                                                                signup: true,
                                                                token: token
                                                                
                                
                                                            });
                                                        }
                                                    });
            
                                                }
                                            })
                                    }
                                })

                            
                            }
                    
                    }
                })


                 

                }
            })
        
})


// Logging in
app.post('/api/web/login', (req, res)=>{

    var email = req.body.email;

    UserModel.findOne({email:email}).populate('organisations').exec( (err,successful)=>{
        if(err){

            res.json({
                login:false,
                message: 'this Email doesnot exist'
            });
        } else if(!successful){

            res.json({
                login:false,
                message: 'this Email doesnot exist'
            });

        } else {
            successful.comparePassword(req.body.password, function(err, isMatch){
			
				if(isMatch && !err){
					var ID = successful._id;
						if(err){
							res.json({
                                login:false
                            })
						} else {
                            jwt.sign({ID}, secretkey, { expiresIn: '12h' }, (err,token)=>{
                                if(err){
                                    res.json({
                                        login:false
                                    });
                                } else {

                                    res.json({
                                        login: true,
                                        token: token,
                                        data: successful
                                        
        
                                    });
                                }
                            });

							
						}
				
				} else if(!isMatch){
					res.json({
						login: false,
						message: 'passsword does not match'
					});
				}
			});
        }
    });

});

app.post('/api/web/addTask', (req,res)=>{

  var token = req.headers.authorization;



  if(token) {

    var jwtToken = token.slice(6, token.length).trimLeft();


  // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    } else {

        if (req.body.engravedNumberSearch !== '' && req.body.inventoryNumberSearch === ''){

            OrganisationModel.findById({_id: req.body.organisationID})
            .populate({path: 'equipment', match: { 'engravedNumber' : req.body.engravedNumberSearch}})
            .exec((err,found)=>{
                if (err) {
                    res.json({
                      save: false
                    })
                } else if (found.equipment.length === 0) {

                   

                  res.json({
                      engravedFound: false
                    })
                }
                
                
                else if (found.equipment.length > 0) {

                   

                    var equipmentID = found.equipment[0]._id;

                    var id = decoded.ID;


                    var taskModel = new TaskModel();
            
                    taskModel.taskType = req.body.taskType;
                    taskModel.desc = req.body.description;
                    taskModel.priority = req.body.priority;
                    taskModel.due = req.body.dueDate;
                    taskModel.date = req.body.issuedDate;
                    taskModel.engravedNumber = req.body.engravedNumberSearch;
                    taskModel.inventoryNumber = req.body.inventoryNumberSearch;
                    taskModel.engineer = req.body.technician;
                    taskModel.assignedBy = id;
            
                    taskModel.save((err, saved)=>{
                        if (err){
            
                        } else if (saved){
            
                            var taskID = saved._id;
            
                            var updating ={
                                $push: {pendingTasks: taskID}
                            }

                            var updatingEquipment = {

                                $push: {tasks: taskID}
                            }
            
                            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                updating, {upsert: true, new: true}, (err,saved) => {
            
                                    if(err) {
            
                                    } else {

                                        EquipmentModel.findByIdAndUpdate({_id: equipmentID},
                                            updatingEquipment, {upsert:true, new:true}, (err,saved) => {

                                                if (err){

                                                } else if (saved) {

                                                    res.json({
                                                        status: true,
                                    
                                                    });

                                                }
                                            })
                                        
                                        
                                    }
            
                                })
            
                          
            
                        }
                    });



                    
                }
            });
            

        } else if (req.body.inventoryNumberSearch !== '' && req.body.engravedNumberSearch === '') {

            OrganisationModel.findById({_id: req.body.organisationID})
            .populate({path: 'equipment', match: { 'inventoryNumber' : req.body.inventoryNumberSearch}})
            .exec((err,found)=>{
                if (err) {
                    res.json({
                      save: false
                    })
                } else if (found.equipment.length === 0) {

                   

                  res.json({
                      inventoryFound: false
                    })
                }
                
                
                else if (found.equipment.length > 0) {

                   

                    var equipmentID = found.equipment[0]._id;

                    var id = decoded.ID;


                    var taskModel = new TaskModel();
            
                    taskModel.taskType = req.body.taskType;
                    taskModel.desc = req.body.description;
                    taskModel.priority = req.body.priority;
                    taskModel.due = req.body.dueDate;
                    taskModel.date = req.body.issuedDate;
                    taskModel.inventoryNumber = req.body.inventoryNumberSearch;
                    taskModel.engravedNumber = req.body.engravedNumberSearch;
                    taskModel.engineer = req.body.technician;
                    taskModel.assignedBy = id;
            
                    taskModel.save((err, saved)=>{
                        if (err){
            
                        } else if (saved){
            
                            var taskID = saved._id;
            
                            var updating ={
                                $push: {pendingTasks: taskID}
                            }

                            var updatingEquipment = {

                                $push: {tasks: taskID}
                            }
            
                            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                updating, {upsert: true, new: true}, (err,saved) => {
            
                                    if(err) {
            
                                    } else {

                                        EquipmentModel.findByIdAndUpdate({_id: equipmentID},
                                            updatingEquipment, {upsert:true, new:true}, (err,saved) => {

                                                if (err){

                                                } else if (saved) {

                                                    res.json({
                                                        status: true,
                                    
                                                    });

                                                }
                                            })
                                        
                                        
                                    }
            
                                })
            
                          
            
                        }
                    });



                    
                }
            });

        } else if (req.body.inventoryNumberSearch === '' && req.body.engravedNumberSearch === '') {
            var id = decoded.ID;


            var taskModel = new TaskModel();
    
            taskModel.taskType = req.body.taskType;
            taskModel.desc = req.body.description;
            taskModel.priority = req.body.priority;
            taskModel.due = req.body.dueDate;
            taskModel.date = req.body.issuedDate;
            taskModel.engineer = req.body.technician;
            taskModel.assignedBy = id;
    
            taskModel.save((err, saved)=>{
                if (err){
    
                } else if (saved){
    
                    var taskID = saved._id;
    
                    var updating ={
                        $push: {pendingTasks: taskID}
                    }
    
                    OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                        updating, {upsert: true, new: true}, (err,saved) => {
    
                            if(err) {
    
                            } else {
                                
                                res.json({
                                    status: true,
                
                                });
                            }
    
                        })
    
                  
    
                }
            });
        }
       
    }
    
  });

  } else if(!token) {

    res.json({
        status: false
    });
  }



});



app.post('/api/web/createEquipmentBasic', (req,res) => {

var token = req.headers.authorization;

  if(token) {

    var jwtToken = token.slice(6, token.length).trimLeft();

    var splitJWTToken = jwtToken.split(' ');

        var chosenToken = splitJWTToken[0];
        var organID = splitJWTToken[1];


  // verify a token symmetric
jwt.verify(chosenToken, secretkey, function(err, decoded) {
    if(err) {

        res.json({
            status: false
        });

    }  else {

        var id = decoded.id;

        var inventoryNumber = req.body.inventoryNumber;

        OrganisationModel.findById({_id: organID})
        .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
        .select('equipment, -_id').exec((err,found)=>{
            if (err) {
                res.json({
                    error: err
                })
            } else if (found){

                if(found.equipment.length === 0) {

                 
                if(!req.body.contactPersonPhone) {
                    

                    var equipmentModel = new EquipmentModel();


                    equipmentModel.equipmentName = req.body.equipmentName;
                    equipmentModel.equipmentModel = req.body.model;
                    equipmentModel.inventoryNumber = inventoryNumber;
                    equipmentModel.engravedNumber = req.body.engravedNumber;
                    equipmentModel.equipmentType = req.body.equipmentType;
                    equipmentModel.serialNumber = req.body.serialNumber;
                    equipmentModel.desc = req.body.equipmentDescription;
                    equipmentModel.physicalLocation = req.body.physicalLocation;
                    equipmentModel.departmentLocation = req.body.departmentLocation;
                    equipmentModel.departmentContactPerson = req.body.departmentConctactPerson;
                    equipmentModel.contactPersonCode = '';
                    equipmentModel.contactPersonNumber = '';
                    equipmentModel.serviceRequirements = req.body.serviceRequirement;
                    equipmentModel.maintenanceRequirements = req.body.maintenanceRequirement;
                    equipmentModel.equipmentFunction = req.body.equipmentFunction;
                    equipmentModel.physicalRisk = req.body.physicalRisk;
                    equipmentModel.sparesAvailable = req.body.sparesAvailable;
                    equipmentModel.willBeMoved = req.body.willMove;
                    equipmentModel.equipmentCondition = req.body.equipmentCondition;
                    equipmentModel.powerRequirement = req.body.powerRequirement;
                    equipmentModel.currencyCode = req.body.currencyCode;
                    equipmentModel.cost = req.body.purchaseCost;
                    equipmentModel.otherNotes = req.body.otherNotes;
                    equipmentModel.comissionDate = req.body.commissionDate;
                    equipmentModel.technician = req.body.engineer;
                    if(req.body.equipmentUsers.length === 1){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});

                    } else if (req.body.equipmentUsers.length === 2){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});

                    } else if (req.body.equipmentUsers.length === 3){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[2]]});


                    } else if (req.body.equipmentUsers.length === 4){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[2]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[3]]});



                    } 
    

                    equipmentModel.save((err, saved)=>{
                        if (err){
    
            
                            console.log(err)
                        } else if (saved){

                            var equipID = saved._id;
                            
                            var updating = {
                                $push: {equipment: equipID }

                            }

                            OrganisationModel.findByIdAndUpdate({_id: organID},
                                updating, {new: true, upsert: true}, (err, updated)=>{
                                    if (err) {

                                    } else if (updated) {

                                        res.json({
                                            saved: true,
                        
                                        });    
                                    }
                                })

                        
    
            
                        }
                    });


                } else {

                    var equipmentModel = new EquipmentModel();


                    equipmentModel.equipmentName = req.body.equipmentName;
                    equipmentModel.equipmentModel = req.body.model;
                    equipmentModel.inventoryNumber = inventoryNumber;
                    equipmentModel.engravedNumber = req.body.engravedNumber;
                    equipmentModel.equipmentType = req.body.equipmentType;
                    equipmentModel.serialNumber = req.body.serialNumber;
                    equipmentModel.desc = req.body.equipmentDescription;
                    equipmentModel.physicalLocation = req.body.physicalLocation;
                    equipmentModel.departmentLocation = req.body.departmentLocation;
                    equipmentModel.departmentContactPerson = req.body.departmentConctactPerson;
                  equipmentModel.contactPersonCode = req.body.contactPersonPhone.dialCode;
                  equipmentModel.contactPersonCountryCode = req.body.contactPersonPhone.countryCode;
                    equipmentModel.contactPersonNumber = req.body.contactPersonPhone.nationalNumber;
                    equipmentModel.serviceRequirements = req.body.serviceRequirement;
                    equipmentModel.maintenanceRequirements = req.body.maintenanceRequirement;
                    equipmentModel.equipmentFunction = req.body.equipmentFunction;
                    equipmentModel.physicalRisk = req.body.physicalRisk;
                    equipmentModel.sparesAvailable = req.body.sparesAvailable;
                    equipmentModel.willBeMoved = req.body.willMove;
                    equipmentModel.equipmentCondition = req.body.equipmentCondition;
                    equipmentModel.powerRequirement = req.body.powerRequirement;
                    equipmentModel.currencyCode = req.body.currencyCode;
                    equipmentModel.cost = req.body.purchaseCost;
                    equipmentModel.otherNotes = req.body.otherNotes;
                    equipmentModel.comissionDate = req.body.commissionDate;
                    equipmentModel.technician = req.body.engineer;

                    if(req.body.equipmentUsers.length === 1){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});

                    } else if (req.body.equipmentUsers.length === 2){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});

                    } else if (req.body.equipmentUsers.length === 3){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[2]]});


                    } else if (req.body.equipmentUsers.length === 4){
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[0]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[1]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[2]]});
                        equipmentModel.equipmentUsers.push({$each:[req.body.equipmentUsers[3]]});



                    } 
    
    
    
                    equipmentModel.save((err, saved)=>{
                        if (err){
    
                          
            
                        } else if (saved){

                            var equipID = saved._id;
                            
                            var updating = {
                                $push: {equipment: equipID }

                            }

                            OrganisationModel.findByIdAndUpdate({_id: organID},
                                updating, {new: true, upsert: true}, (err, updated)=>{
                                    if (err) {

                                    } else if (updated) {

                                        res.json({
                                            saved: true,
                        
                                        });    
                                    }
                                })

                        }
                    });

                }

                } else if (found.equipment.length > 0) {

                    res.json({
                        saved: false
                    });

                }



            } else if (!found) {
               res.json({
                   error: err
               })
            }
        })


 
    }
    
  });

  } else if(!token) {

    res.json({
        status: false
    });
  }
});

// add manufacturer details below
app.post('/api/web/createEquipmentManufacturer', (req,res) => {

    var token = req.headers.authorization;



    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;

      
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){


                var updatingID = found.equipment[0]._id;
  
                updating = {
                      'manufacturer.name': req.body.manuName,
                      'manufacturer.businessCode': req.body.manuBssCode,
                      'manufacturer.email': req.body.manuEmail,
                      'manufacturer.countryCode': req.body.countryCode,
                      'manufacturer.businessNumber': req.body.manuBssNumber,
                      'manufacturer.fax': req.body.manuFax,
                      'manufacturer.street': req.body.manuStreet,
                      'manufacturer.city': req.body.manuCity,
                      'manufacturer.state': req.body.manuStreet,
                      'manufacturer.country': req.body.manuCountry,
                      'manufacturer.zipPostal': req.body.manuZip,
                      'manufacturer.otherNotes': req.body.manuOtherNotes
                    
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: updatingID}, updating, {upsert: true}, (err,success) => {
                      if(err){
  
  
                      } else if (success) {
  
                          res.json({
                              save: true
                          });
                      }
                  }); 
  
              
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }


})

// add vendor details below
app.post('/api/web/createEquipmentVendor', (req,res) => {

    var token = req.headers.authorization;


    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;
  
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){


                var updatingID = found.equipment[0]._id;

  
                  updating = {
  
                    'vendor.fName': req.body.vendorFirstName,
                    'vendor.surname':  req.body.vendorSurname,
                    'vendor.email':  req.body.vendorEmail,
                    'vendor.company': req.body.vendorCompany,
                    'vendor.BssCode': req.body.vendorBssCode,
                    'vendor.BssCountryCode': req.body.BssCountryCode,
                    'vendor.BssNumber': req.body.vendorBssNumber,
                    'vendor.mobileCountryCode': req.body.mobileCountryCode,
                    'vendor.mobileCode':  req.body.vendorMobileCode,
                    'vendor.mobileNumber':  req.body.vendorMobileNumber,
                    'vendor.fax':  req.body.vendorFax,
                    'vendor.street':  req.body.vendorStreet,
                    'vendor.city': req.body.vendorCity,
                    'vendor.state': req.body.vendorState,
                    'vendor.country':  req.body.vendorCountry,
                    'vendor.zipPostal': req.body.vendorZipPostal,
                    'vendor.otherNotes': req.body.vendorOtherNotes,
                    
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: updatingID}, updating, {upsert: true}, (err,success) => {
                      if(err){
  
  
                      } else if (success) {
  
                          res.json({
                              save: true
                          });
                      }
                  });
  
              
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }
});

app.post('/api/web/createEquipmentServiceVendor', (req,res) => {

    var token = req.headers.authorization;

    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;
  
  
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){


                var updatingID = found.equipment[0]._id;
  
                  updating = {
  
                    'serviceVendor.fName': req.body.vendorFirstName,
                    'serviceVendor.surname':  req.body.vendorSurname,
                    'serviceVendor.email':  req.body.vendorEmail,
                    'serviceVendor.company': req.body.vendorCompany,
                    'serviceVendor.BssCode': req.body.vendorBssCode,
                    'serviceVendor.BssCountryCode': req.body.BssCountryCode,
                    'serviceVendor.BssNumber': req.body.vendorBssNumber,
                    'serviceVendor.mobileCode':  req.body.vendorMobileCode,
                    'serviceVendor.mobileCountryCode': req.body.mobileCountryCode,
                    'serviceVendor.mobileNumber':  req.body.vendorMobileNumber,
                    'serviceVendor.fax':  req.body.vendorFax,
                    'serviceVendor.street':  req.body.vendorStreet,
                    'serviceVendor.city': req.body.vendorCity,
                    'serviceVendor.state': req.body.vendorState,
                    'serviceVendor.country':  req.body.vendorCountry,
                    'serviceVendor.zipPostal': req.body.vendorZipPostal,
                    'serviceVendor.otherNotes': req.body.vendorOtherNotes,
                    
                  };
  
                  EquipmentModel.findByIdAndUpdate({_id: updatingID}, updating, {upsert: true}, (err,success) => {
                      if(err){
  
  
                      } else if (success) {
  
                          res.json({
                              save: true
                          });
                      }
                  });
  
              
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }
});

app.post('/api/web/createEquipmentPMS', (req,res) => {

    var token = req.headers.authorization;

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;
  
  
   
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){


                var updatingID = found.equipment[0]._id;
  
               var pmsModel = new PMSModel();

               pmsModel.created = req.body.PMSServiceDate;
               pmsModel.serviceDesc = req.body.PMSDescription;
               pmsModel.schedule = req.body.PMSserviceScheduleString;
               pmsModel.performedBy = req.body.PMSEngineer;
               pmsModel.remarks = req.body.PMSRemarks;
               pmsModel.nextServiceDate = req.body.PMSnextServiceDate;
               pmsModel.stateOfService = req.body.PMSserviceStateString;

               pmsModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    success.populate('performedBy', function(err,done){
                        if(err){

                        } else if (done) {
                            var id = success._id;


                            EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                                {$push:
                           {'PMS': id}}, {upsert: true, new: true}, (err,suc) =>{
        
                            if(err) {
        
                            } else if (suc){
                                res.json({
                                    save: true,
                                    data: done
                                });
                            }
                           });
                        }
                    })

                  
                   }
               });

  
            
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }

});


var manualUpload = multer({
    storage: multer.diskStorage({
      destination: (req, file, callback) => {
        var path ='./../../_MANUALS_FOLDER'; // relative file path

    
        if (!fs.existsSync(path)){
            fs.mkdirSync(path);
        }
       
        callback(null, path);
      },
      filename: (req, file, callback) => {
        //originalname is the uploaded file's name with extn
        callback(null, file.originalname);
      }
    })
  });


 app.post('/api/web/addManual', manualUpload.single('manual'), (req,res)=>{

   
    var token = req.headers.authorization;

   var inventoryNumber = req.body.inventoryNumber;

   var organID = req.body.organisationID;
         
  
                OrganisationModel.findById({_id: organID})
                .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
                .select('equipment, -_id').exec((err,found)=>{
                    if (err) {

                       
                        res.json({
                          save: false
                        })
                    } 
                    else if (found){

                        if (found.equipment.length === 0) {

                            res.json({
                                save: false
                              })

                        } else if (found.equipment.length > 0) {

            
      
             var updatingID = found.equipment[0]._id
  
               var manualsModel = new ManualsModel();

               var time = new Date();
             
               manualsModel.filename = req.file.filename;
               manualsModel.originalname = req.file.originalname;

               manualsModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    var id = success._id;

                    updateURL = {
                        $set: {manualURL:req.protocol + '://' + req.get('host') + '/api/manuals/' + id,
                    filename: id }
                    };

                    ManualsModel.findByIdAndUpdate({'_id': id}, updateURL, {upsert: true, new: true}, (err,success) =>{

                    if(err) {

                    } else if (success){

                        fs.rename('./../../_MANUALS_FOLDER/' +req.file.originalname,
                         './../../_MANUALS_FOLDER/'+ id  + '.pdf', function(err) {
                           
                            if (err) {
                               // console.log(err)
                            } else {

                                EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                                    {$push:
                               {'manuals': id}}, {upsert: true}, (err,suc) =>{
            
                                if(err) {
            
                                } else if (suc){
                                    res.json({
                                        save: true,
                                        data: success
                                    });
                                }
                               });
                            }
                          

                        });
                 

          


                    }
                   });
                   }
               });

  
                        }
              }
          });
     

});

app.post('/api/web/createEquipmentAccessory', (req,res) =>{

    var token = req.headers.authorization;

    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;
  
  
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){


                var updatingID = found.equipment[0]._id;
  
               var accessoriesModel = new AccessoriesModel();

               accessoriesModel.desc = req.body.desc;
               accessoriesModel.serialNumber = req.body.serialNumber;
               accessoriesModel.modelNumber = req.body.modelNumber;
               accessoriesModel.quantity = req.body.qnty;
               accessoriesModel.name = req.body.name;

               accessoriesModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    var id = success._id;


                    EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                        {$push:
                   {'accessories': id}}, {upsert: true}, (err,suc) =>{

                    if(err) {

                    } else if (suc){
                        res.json({
                            save: true
                        });
                    }
                   });
                   }
               });

  
            
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }

});

// add spare part below
app.post('/api/web/createEquipmentSpare', (req,res) =>{

    var token = req.headers.authorization;

    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;
  
  
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){

                var updatingID = found.equipment[0]._id;

  
               var sparesModel = new SparesModel();

               sparesModel.desc = req.body.desc;
               sparesModel.serialNumber = req.body.serialNumber;
               sparesModel.modelNumber = req.body.modelNumber;
               sparesModel.quantity = req.body.qnty;
               sparesModel.name = req.body.name;

               sparesModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    var id = success._id;


                    EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                        {$push:
                   {'spares': id}}, {upsert: true}, (err,suc) =>{

                    if(err) {

                    } else if (suc){
                        res.json({
                            save: true
                        });
                    }
                   });
                   }
               });

  
            
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }

});

app.post('/api/web/createEquipmentSingleAccessory', (req,res) =>{

    var token = req.headers.authorization;

    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;

  
  
          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){

                var updatingID = found.equipment[0]._id;
  
               var accessoriesModel = new AccessoriesModel();

               accessoriesModel.desc = req.body.desc;
               accessoriesModel.serialNumber = req.body.serialNumber;
               accessoriesModel.modelNumber = req.body.modelNumber;
               accessoriesModel.quantity = req.body.qnty;
               accessoriesModel.name = req.body.name;

               accessoriesModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    var id = success._id;


                    EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                        {$push:
                   {'accessories': id}}, {upsert: true}, (err,suc) =>{

                    if(err) {

                    } else if (suc){
                        res.json({
                            save: true,
                            data: success
                        });
                    }
                   });
                   }
               });

  
            
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }

});

// add single spare below
app.post('/api/web/createEquipmentSingleSpare', (req,res) =>{

    var token = req.headers.authorization;

    if(token) {
  
        var jwtToken = token.slice(6, token.length).trimLeft();

        var splitJWTToken = jwtToken.split(' ');
    
            var chosenToken = splitJWTToken[0];
            var organID = splitJWTToken[1];
  
  
    // verify a token symmetric
  jwt.verify(chosenToken, secretkey, function(err, decoded) {
      if(err) {
  
          res.json({
              status: false
          });
  
      } else {
  
          var id = decoded.id;
  
          var inventoryNumber = req.body.inventoryNumber;

          OrganisationModel.findById({_id: organID})
          .populate({path: 'equipment', match: { 'inventoryNumber' : inventoryNumber}})
          .select('equipment, -_id').exec((err,found)=>{
              if (err) {
                  res.json({
                    save: false
                  })
              } else if (found.equipment.length === 0) {
                res.json({
                    save: false
                  })
              }
              
              
              else if (found.equipment.length > 0){

                var updatingID = found.equipment[0]._id;
  
               var sparesModel = new SparesModel();

               sparesModel.desc = req.body.desc;
               sparesModel.serialNumber = req.body.serialNumber;
               sparesModel.modelNumber = req.body.modelNumber;
               sparesModel.quantity = req.body.qnty;
               sparesModel.name = req.body.name;

               sparesModel.save((err,success) => {
                   if(err) {

                   } else if(success){

                    var id = success._id;


                    EquipmentModel.findByIdAndUpdate({_id: updatingID}, 
                        {$push:
                   {'spares': id}}, {upsert: true, new: true}, (err,suc) =>{

                    if(err) {

                    } else if (suc){
                        res.json({
                            save: true,
                            data: success
                        });
                    }
                   });
                   }
               });

  
            
              }
          });
  
  
  
  
         
      }
      
    });
  
    } else if(!token) {
  
      res.json({
          status: false
      });
    }

});


// get pending task report
app.post('/api/web/pendingTaskReport', (req,res) => {

    var token = req.headers.authorization;

    if (token) {

        var jwtToken = token.slice(6, token.length).trimLeft();
  
  
        // verify a token symmetric
      jwt.verify(jwtToken, secretkey, function(err, decoded) {
          if(err) {
      
              res.json({
                  status: false
              });
      
          } else {


            // checking if both dates are filled in

            if(!req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'pendingTasks',
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.pendingTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'pendingTasks',
                match:{due: {$gte: new Date(convertedFromDate),$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.pendingTasks
                            })
                          
                        }
                    });
                
            } else if (!req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'pendingTasks',
                match:{due: {$gte: new Date(convertedFromDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.pendingTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'pendingTasks',
                match:{due: {$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.pendingTasks
                            })
                          
                        }
                    });

            }
        

           }
        });

    } else if (!token) {

        res.json({
            status: false
        })
    }

   
})

// get finished task report
app.post('/api/web/finishedTaskReport', (req,res) => {

    var token = req.headers.authorization;

    if (token) {

        var jwtToken = token.slice(6, token.length).trimLeft();
  
  
        // verify a token symmetric
      jwt.verify(jwtToken, secretkey, function(err, decoded) {
          if(err) {
      
              res.json({
                  status: false
              });
      
          } else {


            // checking if both dates are filled in

            if(!req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.finishedTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$gte: new Date(convertedFromDate),$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.finishedTasks
                            })
                          
                        }
                    });
                
            } else if (!req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$gte: new Date(convertedFromDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.finishedTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                data: found.finishedTasks
                            })
                          
                        }
                    });

            }
        

           }
        });

    } else if (!token) {

        res.json({
            status: false
        })
    }

   
})

// get both finished and reports 
app.post('/api/web/finishedAndPendingTaskReport', (req,res) => {

    var token = req.headers.authorization;

    if (token) {

        var jwtToken = token.slice(6, token.length).trimLeft();
  
  
        // verify a token symmetric
      jwt.verify(jwtToken, secretkey, function(err, decoded) {
          if(err) {
      
              res.json({
                  status: false
              });
      
          } else {


            // checking if both dates are filled in

            if(!req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .populate({path: 'pendingTasks',
                    populate: {path: 'engineer assignedBy',
                    select: 'fname surname'}})
                        .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {

                         //   console.log(err)
                
                        } else if (found) {
    
                            res.json({
                                dataFinished: found.finishedTasks,
                                dataPending: found.pendingTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$gte: new Date(convertedFromDate),$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .populate({path: 'pendingTasks',
                    match:{due: {$gte: new Date(convertedFromDate),$lt: new Date(convertedToDate)}},
                    populate: {path: 'engineer assignedBy',
                    select: 'fname surname'}})
                        .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                dataFinished: found.finishedTasks,
                                dataPending: found.pendingTasks
                            })
                          
                        }
                    });
                
            } else if (!req.body.toDate && req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$gte: new Date(convertedFromDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .populate({path: 'pendingTasks',
                    match:{due: {$gte: new Date(convertedFromDate)}},
                    populate: {path: 'engineer assignedBy',
                    select: 'fname surname'}})
                        .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                dataFinished: found.finishedTasks,
                                dataPending: found.pendingTasks
                            })
                          
                        }
                    });

            } else if (req.body.toDate && !req.body.fromDate) {

                var miliToDate = Date.parse(req.body.toDate);
                var convertedToDate = new Date(miliToDate);
    
                var miliFromDate = Date.parse(req.body.fromDate);
                var convertedFromDate = new Date(miliFromDate);
                
                OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'finishedTasks',
                match:{due: {$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('finishedTasks')
                    .populate({path: 'pendingTasks',
                match:{due: {$lt: new Date(convertedToDate)}},
                populate: {path: 'engineer assignedBy',
                select: 'fname surname'}})
                    .select('pendingtTasks')
                    .exec( (err, found) =>{
                        if (err) {
                
                        } else if (found) {
    
    
                            res.json({
                                dataFinished: found.finishedTasks,
                                dataPending: found.pendingTasks
                            })
                          
                        }
                    });

            }
        

           }
        });

    } else if (!token) {

        res.json({
            status: false
        })
    }

})

// searching single equipment below

app.post('/api/web/searchingInventoryNumberReport', (req,res) => {

    var token = req.headers.authorization;

    if (token) {

        var jwtToken = token.slice(6, token.length).trimLeft();
  
  
        // verify a token symmetric
      jwt.verify(jwtToken, secretkey, function(err, decoded) {
          if(err) {
      
              res.json({
                  status: false
              });
      
          } else {

            OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'equipment',
            match:{inventoryNumber: {$eq: req.body.searchString}},
            populate: {path: 'PMS accessories spares', 
            populate: {path: 'performedBy', 
        select:'surname fname'}
        }})
            .select('equipment')
                .exec( (err, found) =>{

                    if (err) {
            
                        console.log(err)
                    } else if (found) {

                   if (found.equipment.length === 0) {

                    res.json({
                        finding: false,
                        message: 'Does not exist in database'
                        
                    })

                   } else if (found.equipment.length > 0) {

                    res.json({
                        finding: true,
                        data: found.equipment,
                       
                    }) 
                   }
                        
                      
                    }
                });

        }
    });

} else if (!token) {

    res.json({
        status: false
    })
}

        
})


// searching single equipment below
app.post('/api/web/searchingEngravedNumberReport', (req,res) => {

    var token = req.headers.authorization;

    if (token) {

        var jwtToken = token.slice(6, token.length).trimLeft();
  
  
        // verify a token symmetric
      jwt.verify(jwtToken, secretkey, function(err, decoded) {
          if(err) {
      
              res.json({
                  status: false
              });
      
          } else {

            OrganisationModel.findById({_id: req.body.institutionID}).populate({path: 'equipment',
            match:{engravedNumber: {$eq: req.body.searchString}},
            populate: {path: 'PMS accessories spares'}})
            .select('equipment')
                .exec( (err, found) =>{

                    if (err) {
            
                        console.log(err)
                    } else if (found) {

                   if (found.equipment.length === 0) {

                    res.json({
                        finding: false,
                        message: 'Does not exist in database'
                        
                    })

                   } else if (found.equipment.length > 0) {

                    res.json({
                        finding: true,
                        data: found.equipment,
                       
                    }) 
                   }
                        
                      
                    }
                });

        }
    });

} else if (!token) {

    res.json({
        status: false
    })
}


})
};