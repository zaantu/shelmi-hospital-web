module.exports.set = function(app) {
    var jwt = require('jsonwebtoken');
    var secretkey = require('../../secretkey');
    var mongoose = require('mongoose');
    var http = require('http');
    const fs = require("fs");
    var path = require('path');




    var ObjectId = mongoose.Types.ObjectId;

    var UsersModel = require('../../models/users');
    var TaskModel =require('../../models/tasks');
    var EquipmentModel = require('../../models/equipment');
    var PMSModel = require('../../models/PMS');
    var ManualsModel = require('../../models/manuals');
    var PendingTasksModel = require('../../models/tasks');
    var FinishedTasksModel = require('../../models/finishedTasks');
    var OrganisationModel = require('../../models/organisation');
    var TemporaryUsersModel = require('../../models/temporaryusers');

        ////// Verify Email below ///////////
app.get('/api/web/confirmation/:token', (req,res)=>{

    var tokenID = req.params.token;

    jwt.verify(tokenID, secretkey, (err,decoded)=> {
         if (err) {


url = req.protocol + '://' + req.get('host') + '/not-verified/' + ID;
            
             res.redirect(url);
             
         }

         else if (decoded){

             var ID = decoded.ID;

             console.log(decoded)

url = req.protocol + '://' + 'localhost:4200' + '/verify-token/' + ID;
             res.redirect(url);
         }
    })

});

////////////// GET VERIFICATION EMAIL BELOW //////////////////

app.get('/api/web/basic-verify-information/:id', function( req,res)  {

    TemporaryUsersModel.findById({_id: req.params.id}, (err, found) => {
        if (err) {



        } else if (found) {
            res.json({
                finding: true,
                data: found
            })
        } else if(!found) {
            res.json({
                finding:false
            })
        }
    })
    

})

///////////////////////////////////////////////////////////

//////////// CHECKING FOR USERS OF ORGANISATION BELOW ///////////////////////// 
app.get('/api/web/users/:id', function(req,res){

    OrganisationModel.findById({_id: req.params.id})
    .populate({path: 'users', 
    select:'-password -modified -joined'})
    .exec( (err, found)=>{
        if(err) {

        } else if (found) {

            res.json({
                data: found.users,
                foundData: true,


            });

        }
    });
 
   


});

///////////////////////////////////////////////////////////////

////////////////// GETTING PENDING USERS FOR ORGANISATION //////////////////////
app.get('/api/web/getPendingUsers/:id', function(req,res){

    OrganisationModel.findById({_id: req.params.id})
    .populate({path: 'temporaryUsers'})
    .exec( (err, found)=>{
        if(err) {

        } else if (found) {

            res.json({
                data: found.temporaryUsers,
                foundData: true,


            });

        }
    });
});



////////////////////////////////////////////////////////////////////////////////


////////////// READ MANUAL BELOW /////////////
app.get('/api/web/readManual/:id', (req,res) => {
    var id = req.params.id;


  ManualsModel.findById(id, (err, pdf) => {
    if (err) {
        console.log(err);
    } else if (pdf) {
            // stream the image back by loading the file
 res.setHeader('Content-Type', 'application/pdf');

    fs.createReadStream(path.join('./../../_MANUALS_FOLDER', pdf.filename + '.pdf'))
    .pipe(res);

  

    }

});


    
})




////////////// GET TASKS BELOW ///////////// 
app.get('/api/web/getPendingTasks/:page', function(req,res){

    var perPage = 10;
    var page = req.params.page || 1;

    var token = req.headers.authorization;

    if (token) {

        var institutionID = token.slice(6, token.length).trimLeft();

    OrganisationModel.findById({_id: institutionID})
    .populate({path: 'pendingTasks', options: { skip :(perPage * page) - perPage, limit: perPage},
    populate: {path: 'engineer assignedBy', select: 'fname surname'}})
        .select('pendingTasks')
        .sort({date:1}).lean()
        .exec( (err, found) =>{
            if (err) {
                res.json({

                    error: true
                })
    
            } else if (found) {

            OrganisationModel.aggregate([

                { $match : { _id : ObjectId(institutionID)} },
                // unwind the history ar
                {$unwind: '$pendingTasks'},
                
                // count the result
                {$group: {_id: '$_id', count: {$sum: 1}}}
             ], function(err, doc) {
                 if (err){
        
                 } else if (doc) {

                    if (doc.length === 0 ) {

                        res.json({
                            found: false
                        })

                    } else if (doc.length > 0) {

                    
                        pendingNumber = doc[0].count;
    
                        
                        res.json({
                            data: found.pendingTasks,
                            current: page,
                            pages: Math.ceil(pendingNumber / perPage),
                            count: pendingNumber
                        });

                    }

                
        
                 } else if (!doc) {
                     res.json({
                         found: false,
        
                     })
                 }
            
             });

              
            }
        });

    } else if (!token){
        res.json({
            status: false
        });

    }

 
    

    
});

/////// GET FINISHED TASKS BELOW ///////////
app.get('/api/web/getFinishedTasks/:id', function(req,res){

    OrganisationModel.findById({_id: req.params.id}).populate({path: 'finishedTasks',
populate: {path: 'engineer assignedBy', select: 'fname surname'}})
    .select('finishedTasks')
    .exec( (err, found) =>{
        if (err) {

        } else if (found) {
            res.json({
                data: found.finishedTasks
            });
        }
    });
 
    

    
});


// get list of all equipment below ///
app.get('/api/web/getEquipment/:id', function(req,res) {


    OrganisationModel.findById({_id: req.params.id})
    .populate({path: 'equipment', 
    populate: {path: 'accessories spares manuals PMS',
    populate: {path: 'performedBy'}}}).select('equipment , -_id').exec((err, found) => {
        if(err){

            res.send(err);
            res.json({

                
                foundData: false
            });
        } else if (found) {

            res.json({

                foundData: true,
                data: found.equipment

            });
        }
    }) 


});

// get Equipment Number below
app.get('/api/web/getEquipmentNumber/:id', function(req,res) {
    
    OrganisationModel.findById({_id: req.params.id})
    .populate({path: 'equipment',
    path: 'accessories spares manuals PMS',
path: 'performedBy'}).select('equipment , -_id').exec((err, found) => {
        if(err){

            res.send(err);
            res.json({

                
                foundData: false
            });
        } else if (found) {
            res.json({

                foundData: true,
                data: found.equipment

            });
        }
    }) 
});

// get user name for equipment side
app.get('/api/web/getUserNames', function(req,res) {


    var ID = req.params.id;

    var token = req.headers.authorization;
    

    if(token) {
        var jwtToken = token.slice(6, token.length).trimLeft();


          // verify a token symmetric
jwt.verify(jwtToken, secretkey, function(err, decoded) {
    if(err) {



        res.json({
            status: false
        });

    } else if (decoded) {

        var ID = decoded.ID;


        UsersModel.findById({_id: ID},'fname surname role', function(err,success){
            if(err) {
                res.json({
                    
                })
            } else if (success) {
                res.json({
                    data: success
                })
            }
        })
      
    }



    });
     
    }
        else if(!token) {
        res.json({
            status: false
        });

    }

});

// get PMS Single details below
app.get('/api/web/getPMSSingle/:id', function(req,res) {
    var ID = req.params.id;

    PMSModel.findById({_id: ID}, function(err,found) {
        if(err) {

        } else if (found) {
            res.json({
                finding: true,
                data: found
            })
        }
    })
})

app.get('/api/web/getManual/:id', function(req,res){

    var ID = req.params.id;

    PMSModel.findById({_id: ID}, function(err,found) {
        if(err) {

        } else if (found) {
            res.json({
                finding: true,
                data: found
            })
        }
    })

})

// getting finished tasks count
app.get('/api/web/finishedTasksCount/:id', function(req,res) {

    OrganisationModel.aggregate([
        { $match : { _id : ObjectId(req.params.id)} },

        // unwind the history array
        {$unwind: '$finishedTasks'},
        // count the result
        {$group: {_id: '$_id', count: {$sum: 1}}}
     ], function(err, doc) {
         if (err){

            console.log(err);
         } else if (doc) {
             res.json({
                 found: true,
                 data: doc
             })

         } else if (!doc) {
             res.json({
                 found: false
             })
         }
    
     });

 
})

// get number of equipment below

app.get('/api/web/getEquipmentNumbers/:id', function(req,res){

    OrganisationModel.aggregate([

        { $match : { _id : ObjectId(req.params.id)} },
        // unwind the history array
        {$unwind: '$equipment'},
        
        // count the result
        {$group: {_id: req.params.id, count: {$sum: 1}}}
     ], function(err, doc) {
         if (err){

         } else if (doc) {
    
             res.json({
                 found: true,
                 data: doc



             })

         } else if (!doc) {
             res.json({
                 found: false,

             })
         }
    
     });

})

app.get('/api/web/getEquipmentCondition/:id', function(req,res){


    OrganisationModel.aggregate([

        { $match : { _id : ObjectId(req.params.id)} },
        // unwind the history array
        {$unwind: '$equipment'},
        
        // count the result
        {$group: {_id: req.params.id, count: {$sum: 1}}}
     ], function(err, doc) {
         if (err){

         } else if (doc) {
             res.json({
                 found: true,
                 data: doc

             })

         } else if (!doc) {
             res.json({
                 found: false,

             })
         }
    
     });

})

// getting pending tasks count
app.get('/api/web/pendingTasksCount/:id', function (req,res){

    OrganisationModel.aggregate([

        { $match : { _id : ObjectId(req.params.id)} },
        // unwind the history array
        {$unwind: '$pendingTasks'},
        
        // count the result
        {$group: {_id: req.params.id, count: {$sum: 1}}}
     ], function(err, doc) {
         if (err){

         } else if (doc) {
             res.json({
                 found: true,
                 data: doc

             })

         } else if (!doc) {
             res.json({
                 found: false,

             })
         }
    
     });

});


};