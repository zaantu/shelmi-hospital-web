module.exports.set = function(app) {

    var jwt = require('jsonwebtoken');
    var fs = require('fs-extra');
    var multer = require('multer');
    const request = require('request');
    var secretkey = require('../../../secretkey');


    var ClientModel = require('../../../models/clients');
    var VisitsModel = require('../../../models/visits');
    var CountryModel = require('../../../models/country')
    var OrganisationModel = require('../../../models/organisation');
    var UserModel = require('../../../models/users');


    ////////////// registering hospital ///////////////////////////////////////

    app.post('/api/hospital/organisation', (req,res) => {


      CountryModel.findOne({country: req.body.country}, (err,found) => {
        if(err) {

        } else if (!found) { // if country name is not found

          var countryModel = new CountryModel();

          countryModel.country = req.body.country;
      
          countryModel.save((err,saved) => {
              if (err) {
      
              } else if (saved) {
                  var ID = saved._id;
      
      
                 OrganisationModel.findOne({name: req.body.organisation}, (err,found)=>{
                     if (err) {
      
      
                     } else if (!found) {
      
                      var organisationModel = new OrganisationModel();
                      organisationModel.name = req.body.organisation;
      
                      organisationModel.save((err,saved)=>{
                          if (err){
      
                          } else if (saved) {

                              var organisationID = saved._id;
                              var organisationName = saved.name;


                              var updating ={
                                  $push:
                                      {'organisation': organisationID}
                              }
      
                              CountryModel.findOneAndUpdate({country: req.body.country},
                                  updating, {new: true, upsert: true}, (err,saved) =>{
                                      if (err){
      
                                      } else if (saved) {
                                          res.json({
                                              success: true,
                                              organisationID: organisationID,
                                              organisationName: organisationName
                                          })
                                      }
                                  })
      
                          }
                      })
      
                     } else if (found) {
      
                      res.json({
                          success: true

                          
                      })
      
                     }
                 })
              }
          })

        } else if (found) { // if country name is found


          OrganisationModel.findOne({name: req.body.organisation}, (err,found)=>{
              if (err) {


              } else if (!found) {

               var organisationModel = new OrganisationModel();
               organisationModel.name = req.body.organisation;


               organisationModel.save((err,saved)=>{
                   if (err){

                   } else if (saved) {

                      var organisationID = saved._id;
                      var organisationName = saved.name;



                      var updating ={
                          $push:
                              {'organisation': organisationID}
                      }

                      CountryModel.findOneAndUpdate({country: req.body.country},
                          updating, {new: true, upsert: true}, (err,saved) =>{
                              if (err){

                              } else if (saved) {
                                  res.json({
                                      success: true,
                                      organisationID: organisationID,
                                      organisationName: organisationName
                                  })
                              }
                          })

                   }
               })

              } else if (found) {

               res.json({

                  success: true
                   
               })

              }
          })
        }
    })
    })

    /////////////////////////////////////////////////////////////////////////

    /////////////////// registering new user for hospital ////////////////////////////
  app.post('/api/hospital/admin-signup', (req,res) => {

       UserModel.findOne({email: req.body.email}, (err,found) =>{

        if (err){

            console.log(err);

        } else if (found) {
            res.json({
                emailExists: true
            })
        } else if (!found) {


                    var userModel = new UserModel();

                    userModel.fname = req.body.fname;
                    userModel.surname = req.body.surname;
                    userModel.email = req.body.email;
                    userModel.role = 'Super Admin';
                    userModel.password = req.body.password;
                    userModel.organisations.push(req.body.organisationID);

                    userModel.save((err,saved) => {
                        if (err) {

                        } else if (saved) {

                            var ID = saved._id;

                        
                            var updating = {
                                $push:
                                {'users': ID}
                            }

                            OrganisationModel.findByIdAndUpdate({_id: req.body.organisationID},
                                updating, {upsert: true, new : true}, (err,updated)=>{
                                    if(err) {
                                         
                                    } else if (updated) {


                                        jwt.sign({ID}, secretkey, { expiresIn: '12h' }, (err,token)=>{
                                            if(err){
                                                res.json({
                                                    login:false
                                                });
                                            } else {
            
                                               // res.cookie('jwt',token, { httpOnly: false, secure: false, path: '/', maxAge: 720000000 });
            
                                                res.json({
                                                    signup: true,
                                                    token: token
                                                    
                    
                                                });
                                            }
                                        });

                                    }
                                })
                        }
                    })

                    
                
            }

        
    }) 
        
    })


    //////////////////////////////////////////////////////////////////////

    /////////////// login into hospital ////////////////////////////////////


    app.post('/api/hospital/login', (req,res) => {

        var email = req.body.email;

        UserModel.findOne({email:email}).populate('organisations').exec( (err,successful)=>{
            if(err){
    
                res.json({
                    login:false,
                    message: 'this Email doesnot exist'
                });
            } else if(!successful){
    
                res.json({
                    login:false,
                    message: 'this Email doesnot exist'
                });
    
            } else {
                successful.comparePassword(req.body.password, function(err, isMatch){
                
                    if(isMatch && !err){
                        var ID = successful._id;
                            if(err){
                                res.json({
                                    login:false
                                })
                            } else {
                                jwt.sign({ID}, secretkey, { expiresIn: '12h' }, (err,token)=>{
                                    if(err){
                                        res.json({
                                            login:false
                                        });
                                    } else {
    
                                        res.json({
                                            login: true,
                                            token: token,
                                            data: successful
                                            
            
                                        });
                                    }
                                });
    
                                
                            }
                    
                    } else if(!isMatch){
                        res.json({
                            login: false,
                            message: 'passsword does not match'
                        });
                    }
                });
            }
        });


    })
    ///////////////////////////////////////////////////////////////////////

    ///////////// adding patient below //////////////////////////////////

    var patPicUpload = multer({
        storage: multer.diskStorage({
          destination: (req, file, callback) => {
            var path ='./../../_PATIENT_PICTURES'; // relative file path
    
        
            if (!fs.existsSync(path)){
                fs.mkdirSync(path);
            }
           
            callback(null, path);
          },
          filename: (req, file, callback) => {
            //originalname is the uploaded file's name with extn
            callback(null, file.originalname);
          }
        })
      });

    app.post('/api/hospital/pat-details-one', (req,res) => {


        console.log(req.body.patientID);

    });
    


    /////////////////////////////////////////////////////////////////////

 
};