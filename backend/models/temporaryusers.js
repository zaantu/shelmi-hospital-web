var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsersSchema = new Schema({
  

    emailVerified: {type: Boolean, default: false},
    role : {type: String},
    email: {type: String, unique: true},
    token: String,
    organisations: [{type: Schema.Types.ObjectId, ref :'organisations'}],
    branches: [{type: Schema.Types.ObjectId, ref :'branches'}],
	joined: { type: Date, default: Date.now },
});

module.exports = mongoose.model('temporaryusers', UsersSchema);