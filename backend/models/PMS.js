var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PMSSchema = new Schema({
 
    serviceDesc: String,
    schedule: String,
    performedBy: {type: Schema.Types.ObjectId, ref :'users' },
    validationBy: String,
    validationDate: String,
    remarks: String,
    nextServiceDate: Date,
    stateOfService: String,
   created: {type: Date},

 
});


module.exports = mongoose.model('pms', PMSSchema);