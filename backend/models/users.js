var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var UsersSchema = new Schema({
    fname: String,
    surname: String,
    realnumber: {type: String},
    number: {type: Number},
    numberVerified: {type: Boolean, default: false},
    role : {type: String},
    subRole: {type: String},
    code: {type:String},
    permitted: {type:Boolean, default:false},
    email: {type: String, unique: true},
    organisations: [{type: Schema.Types.ObjectId, ref :'organisations'}],
    branches: [{type: Schema.Types.ObjectId, ref :'branches'}],
    dp: String,
    password: String,
	joined: { type: Date, default: Date.now },
});

// store user's hashed password
UsersSchema.pre('save', function(next){

    

    var user = this;
    if(this.isModified('password') || this.isNew){
    bcrypt.genSalt(10, function(err, salt) {
    
    if(err){
    return next(err); 
    }

    bcrypt.hash(user.password, salt, function(err,hash){
    if(err){
        return next(err);
    }
    user.password = hash;
    next();
    
    });
    
    });
    
    
    }
    
    else {
    
        return next();
    }
    
    });
    
    //method to compare the password
    UsersSchema.methods.comparePassword = function(pw, cb){
    
         bcrypt.compare(pw, this.password, function(err, isMatch){
    if(err){
        return cb(err);
    }
    
    cb(null, isMatch);
         });
    };


module.exports = mongoose.model('users', UsersSchema);