var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CountrySchema = new Schema({

    country:{
        type:String,
       
    },
    organisation:[{
        type: Schema.Types.ObjectId, ref :'organisations',
    }]
   
});


module.exports = mongoose.model('countries', CountrySchema);