var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VisitsSchema = new Schema({
 

    patiendID: {type: String, unique:true},
    fname: String,
    surname: String,
    middleName: String,
    contactNumber: { type: String},
    email: {type: String},
    dob: {type: Date},
    age: {type:Number},
    bloodGroup: {type: String},
    address: String,
    dp: String,
    maritalStatus: String,
    allergies: String,
    severety: {type: String},
    client : {type: Schema.Types.ObjectId, ref :'clients'},
    users: {type: Schema.Types.ObjectId, ref :'users'},
    date: {type:Date, default: Date.now},
    organisation: {type: Schema.Types.ObjectId, ref: 'organisations'},
 
});



module.exports = mongoose.model('visits', VisitsSchema);
