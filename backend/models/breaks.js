var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BreaksSchema = new Schema({
    cause: String,
    desc: String,
    equipment : {type: Schema.Types.ObjectId, ref :'equipment'},
    created: {type:Date, default: Date.now},
    reportedBy: {type: Schema.Types.ObjectId, ref :'users'},
    till: {type:Date},
 
});



module.exports = mongoose.model('breaks', BreaksSchema);
