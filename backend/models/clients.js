var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientsSchema = new Schema({
 

    contact1: {type:String, unique: true},
    contact2: {type:String, unique: true},
    email: {type: String, unique: true},
    date: {type:Date, default: Date.now},
    visits :[
        {type: Schema.Types.ObjectId, ref :'visits'}

    ]
});



module.exports = mongoose.model('clients', ClientsSchema);
