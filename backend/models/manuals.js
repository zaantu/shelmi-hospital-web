var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var manualSchema = new Schema({

    filename:String,
    originalname: String,
    manualURL: String,
	joined: { type: Date, default: Date.now },
});


module.exports = mongoose.model('manuals', manualSchema);