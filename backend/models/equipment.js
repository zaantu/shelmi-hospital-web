var mongoose = require('mongoose');
var async = require('async');

var Schema = mongoose.Schema;

var Accessory = require('./accessories');

var equipmentSchema = new Schema({
    
    equipmentName: String,
    inventoryNumber: String,
    engravedNumber: String,
    equipmentType: String,
    serialNumber: String,
    desc: String,
    physicalLocation: String,
    departmentLocation: String,
    departmentContactPerson: String,
    contactPersonCode: String,
    contactPersonCountryCode: String,
    contactPersonNumber: String,
    equipmentModel: String,
    serviceRequirements: String,
    maintenanceRequirements: String,
    equipmentFunction: String,
    physicalRisk: String,
    sparesAvailable: String,
    willBeMoved: String,
    equipmentCondition: String,
    powerRequirement: String,
    currencyCode: String,
    cost: String,
    otherNotes: String,
    equipmentUsers: [
        {type: String}
    ],
    comissionDate: String,
    technician:  {type: Schema.Types.ObjectId, ref :'users'},
    created: {type:Date, default: Date.now},
    manufacturer: {
        name: {type: String},
        email: {type: String},
        businessCode: {type: String},
        countryCode: {type: String},
        businessNumber: {type: String},
        fax: {type:String},
        street: {type: String},
        city: {type: String},
        state: {type: String},
        country: {type: String},
        zipPostal: {type: String},
        otherNotes: {type: String}
    },
    vendor: {
        fName: {type: String},
        surname: {type: String},
        email: {type: String},
        company: { type: String},
        BssCode: {type: String},
        BssCountryCode: {type: String},
        BssNumber: {type: String},
        mobileCode: {type: String},
        mobileCountryCode: {type: String},
        mobileNumber: {type: String},
        fax: {type: String},
        street: {type: String},
        city: {type: String},
        state: {type: String},
        country: {type:String},
        zipPostal: {type: String},
        otherNotes: {type: String}
    },
    serviceVendor: {
        fName: {type: String},
        surname: {type: String},
        email: {type: String},
        company: { type: String},
        BssCode: {type: String},
        BssCountryCode: {type: String},
        BssNumber: {type: String},
        mobileCode: {type: String},
        mobileCountryCode: {type: String},
        mobileNumber: {type: String},
        fax: {type: String},
        street: {type: String},
        city: {type: String},
        state: {type: String},
        country: {type:String},
        zipPostal: {type: String},
        otherNotes: {type: String}
    },
    PMS : [
     {type: Schema.Types.ObjectId, ref :'pms'},


    ],

    manuals: [
        {
            type: Schema.Types.ObjectId, ref :'manuals'
        }
    ],
    accessories: [
        {
            type: Schema.Types.ObjectId, ref :'accessories'
        }
    ],
    spares: [
        {
            type: Schema.Types.ObjectId, ref :'spares'
        }
    ],
    tasks: [
        {
            type: Schema.Types.ObjectId, ref :'tasks'

        }
    ],

    finishedTasks: [
        {
            type: Schema.Types.ObjectId, ref :'finishedTasks'

        }
    ],
    breaks: [
        {
            type: Schema.Types.ObjectId, ref :'breaks' 
        }
    ]
       
        
    

 
});




 



module.exports = mongoose.model('equipment', equipmentSchema);
