var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedbackSchema = new Schema({

 
    user: {
        type: Schema.Types.ObjectId, ref :'users'},
    message: {type: String},
    created: { type: Date, default: Date.now },
});


module.exports = mongoose.model('feedback', FeedbackSchema);