var mongoose = require('mongoose');
//var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var BranchSchema = new Schema({

    name: {type:String},
    town:{type:String},
    street: {type: String},
    paidPlan:{type:Boolean, default: false},
        expires: {type: Date },
        duration: String,
        paidfor: Number,
        users: [
            {type: Schema.Types.ObjectId, ref :'users'},
          
        ],
    userNumber: {type: Number},
    organisation :
    { type: Schema.Types.ObjectId, ref :'organisations', },

    joined: { type: Date, default: Date.now },

  
});


module.exports = mongoose.model('branches', BranchSchema);