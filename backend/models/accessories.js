var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AccessoriesSchema = new Schema({
    name: String,
    desc: String,
    serialNumber: {type: String},
    modelNumber : {type: String},
    quantity: {type: Number},
    date: {type:Date, default: Date.now},

 
});



module.exports = mongoose.model('accessories', AccessoriesSchema);
