var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IssuesSchema = new Schema({

 
    user: {
        type: Schema.Types.ObjectId, ref :'users'},
    message: {type: String},
    openTicket: {type: Boolean, default: true},
    created: { type: Date, default: Date.now },
});


module.exports = mongoose.model('issues', IssuesSchema);