var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrganisationSchema = new Schema({

    name:{type:String},


        module: [

        {name: String,
        paidPlan:{type:Boolean, default: false},
        paidExpires: {type: Date },
        paidDuration: String}
        
        ],
        ultimatePackage: {type: Boolean, default: false},
        
        users: [
            {type: Schema.Types.ObjectId, ref :'users'},
          
        ],
        temporaryUsers :[
            {type: Schema.Types.ObjectId, ref :'temporaryusers'},

        ],
        equipment: [
            {type: Schema.Types.ObjectId, ref :'equipment'},

        ],
        pendingTasks: [
            {type: Schema.Types.ObjectId, ref :'tasks'},

        ],

        finishedTasks: [
            {type: Schema.Types.ObjectId, ref :'finishedTasks'},

        ],

    hasBranches: {type: Boolean},

    branches: [{  
            
       type: Schema.Types.ObjectId, ref :'branches',
            
        }],


	joined: { type: Date, default: Date.now },
});


module.exports = mongoose.model('organisations', OrganisationSchema);