var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var KinsSchema = new Schema({
 
 
    kinFname: String,
    kinSurname: String,
    kinContact: {type: String, unique: true},
    kinEmail: {type: String, unique: true},
    kinRelationship: {type: String},
    client : {type: Schema.Types.ObjectId, ref :'clients'},
    date: {type:Date, default: Date.now},
    organisation: {type: Schema.Types.ObjectId, ref: 'organisations'},
 
});



module.exports = mongoose.model('kins', KinsSchema);
