var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TasksSchema = new Schema({
    taskType: String,
    desc: String,
    priority: {type: String},
    engineer : {type: Schema.Types.ObjectId, ref :'users'},
    assignedBy: {type: Schema.Types.ObjectId, ref :'users'},
    date: {type:Date, default: Date.now},
    inventoryNumber: String,
    engravedNumber: String,
    due: {type:Date},
 
});



module.exports = mongoose.model('tasks', TasksSchema);
