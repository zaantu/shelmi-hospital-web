
var express = require('express');
var app = express();
var https = require('https');
var http = require('http');
//const server = http.createServer(app)
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var port = 5920;


helmet = require("helmet");
var fs = require('fs');
var cors = require('cors');
var methodOverride = require('method-override');
var morgan = require('morgan');
var fs = require('fs-extra');
var multer = require('multer');
var path = require('path');
var cookieParser = require('cookie-parser');
var socketIO = require('socket.io');

var db = 'mongodb://localhost:27017/shelmi-suite';
mongoose.Promise = global.Promise; 
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(db, { useNewUrlParser: true });

// dealing with the connection just right below 
var dbs = mongoose.connection;
 dbs.on('error', console.error.bind(console, 'connection error:'));
 dbs.once('open', function(){
 	console.log('successfully connected');
 });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(morgan('dev'));
app.use(cookieParser())
app.engine('html', require('ejs').renderFile);

app.use(methodOverride());
app.use(cors());
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Expose-Headers', 'X-Requested-With,content-type, Authorization, access_token');
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT, POST, GET');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

   next();
});


// FOR THE CERTIFICATE OF THE SSL

/*const options = {
   key: fs.readFileSync('/etc/letsencrypt/live/zaantu.com/privkey.pem'),
   cert: fs.readFileSync('/etc/letsencrypt/live/zaantu.com/fullchain.pem')}; */




app.get('/', (req,res)=>{
	res.send('Welcome to Shelmi Hospital');
});





/// IMPORTING ALL THE ROUTES BELOW /////////
var allPosts = require('./controllers/shelmi-hospital/PostRoutes/index');
allPosts.set(app);

var allGets = require('./controllers/shelmi-hospital/GetRoutes/index');
allGets.set(app);

var allDeletes = require('./controllers/shelmi-hospital/DeleteRoutes/index');
allDeletes.set(app);

var allUpdates = require('./controllers/shelmi-hospital/UpdateRoutes/index');
allUpdates.set(app);

/////////////////////////////////////////////////////

//APP LISTENING TO PORT ///////////////////////////////////
server = app.listen(port,function(){
	console.log('app listening on port' + port);
}); 

const io = socketIO(server); // connecting socket io to a server

io.on( 'connection', function(socket) {

   console.log( "User connected..!" );

   socket.on('foo', () => {
      io.emit('foo', 'foo-event');
    });

 
});


app.set('io',io);
/*
const server = https.createServer(options,app).listen(5900);
const io = socketIO(server); */


