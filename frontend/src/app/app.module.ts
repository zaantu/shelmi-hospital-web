
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import {WebcamModule} from 'ngx-webcam';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';


import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';

import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';
import { AdminDepartmentsComponent } from './admin-departments/admin-departments.component';
import { AdministrationStaffComponent } from './administration-staff/administration-staff.component';
import { BirthDeathRecordsComponent } from './birth-death-records/birth-death-records.component';
import { PatientSummaryComponent } from './patient-summary/patient-summary.component';
import { BirthRecordsComponent } from './birth-records/birth-records.component';
import { DeathRecordsComponent } from './death-records/death-records.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { PatientsService } from './patients.service';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppSidebarComponent,
    AdminDepartmentsComponent,
    AdministrationStaffComponent,
    BirthDeathRecordsComponent,
    PatientSummaryComponent,
    BirthRecordsComponent,
    DeathRecordsComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    MatSelectCountryModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    WebcamModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes, {
      onSameUrlNavigation: 'reload'
    }),
    NgxIntlTelInputModule
  ],
  providers: [
    PatientsService,
    CookieService,
    Title,

    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
