import { Component, OnInit } from '@angular/core';
import * as uuid from 'uuid';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {WebcamImage} from 'ngx-webcam';
import {Subject, Observable} from 'rxjs';
import { Country } from '@angular-material-extensions/select-country';
import { CookieService } from 'ngx-cookie-service';
import { PatientsService } from '../patients.service';


interface nextOfKin {
  value: string;
}

interface triageClass {
  value: String;
}

interface patientDepartment {
  value: string;
}

interface searchPatientDepartment {
  value: string;
}

interface payments {
  value: string;
}

interface nextDepartment {
  value: string;
}

interface bloodGroup {
  value: string;
}

interface martialStatus {
  value: string;
}
@Component({
  selector: 'app-patient-summary',
  templateUrl: './patient-summary.component.html',
  styleUrls: ['./patient-summary.component.css']
})
export class PatientSummaryComponent implements OnInit {

  myId = uuid.v4(); /// creating new unique ID here....

  browseImage = true;
  activateWebCam = false;
  retakeSnapShot = false;
  activateWebCamOnce = false;
  
  public webcamImage: WebcamImage = null;
     // webcam snapshot trigger
     private trigger: Subject<void> = new Subject<void>();

     //////// for Appointment tab below /////////////////////
  appointmentPicker: any;

     //////////////////////////////////////////////////////////////////


     ///////////////////// for Patient Visit Tab below ///////////////////////////////////////////
  patientVisitPicker: any;
     ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     ////////////// for patient details tab below ////////////////


     patientCountry: any;

     patAge: any = null;
     patientID = '';
     patientDepartmentSelect = '';
     patFname = '';
     patSurname = '';
     patMiddleName = '';
     patEmail = '';
     patContact = '';
     patDOB: any = null;
     selectedBloodGroup = '';
     marit = '';
     patAddress = '';
     patAllergies = '';
     patRemarks = '';
     kinFname = '';
     kinSurname = '';
     kinRelationship = '';
     kinEmail = '';
     files: File;
     lastFile: any;
     patPicture: any;
   
     originalEmptyImage = '../../assets/images/photo.png';
     paymentOptions = '';
     goingDepartment = '';
   
    
   
           // contact numbers
           separateDialCode = true;
           SearchCountryField = SearchCountryField;
           TooltipLabel = TooltipLabel;
           CountryISO = CountryISO;
           
           phonePatForm = new FormGroup({
             phone: new FormControl('')
           });
   
           phoneKinForm = new FormGroup({
             phone: new FormControl('')
           });

     ////////////////////////////////////////////////////////////////////////////////////
     
   

        kins: nextOfKin[] = [
          {value: 'Spouse'},
          {value: 'Child (in no particular order)'},
          {value: 'Parent'},
          {value: 'Sibling'},
          {value: 'Grand Child'},
          {value: 'Grand Parent'},
          {value: 'Nephew/Niece'},
          {value: 'Uncle/Aunt'},
          {value: 'Cousin'}
                ];

                patientDepartments: patientDepartment[] = [
                  {value: 'OPD - Out Patient'},
                  {value: 'IPD - In Patient'}
                ];

                payOption: payments[] = [
                  {value: 'Cash'},
                  {value: 'Insurance'},
                  {value: 'Mobile Money'}
                ];

                triageClass: triageClass[] = [
                  {value: 'Referral'},
                  {value: 'Emergency'},
                  {value: 'Birth Delivery'},
                  {value: 'Walk-in'},
                  {value: 'Accident'},

                ];

                nextDepartment: nextDepartment[] = [
                  {value: 'Laboratory'},
                  {value: 'Radiology'},
                  {value: 'Pharmacy'},
                  {value: 'Doctor Apppointment'}
                ];

                bloodGroups: bloodGroup[] = [
                  {value: 'A+'},
                  {value: 'A-'},
                  {value: 'B+'},
                  {value: 'B-'},
                  {value: 'AB+'},
                  {value: 'AB-'},
                  {value: 'O+'},
                  {value: 'O-'},
                ];

                maritalStatus: martialStatus[] = [
                  {value: 'Single'},
                  {value: 'Married'},
                  {value: 'Widowed'},
                  {value: 'Divorced'},

                ];

                searchPatient: searchPatientDepartment[] = [
                  {value: 'All Patients'},
                  {value: 'OPD - Out Patient'},
                  {value: 'IPD - In Patient'}
                ];

  constructor(public snackBar: MatSnackBar, private cookieService: CookieService,
    private patientService: PatientsService /*public webcamImage: WebcamImage*/) {
    
   }

  

  ngOnInit(): void {


    this.patientID = this.myId;
  }

    triggerSnapshot(): void {

      this.retakeSnapShot = true;
      this.activateWebCamOnce = false;
      this.trigger.next();
     }


     handleImage(webcamImage: WebcamImage): void {
      console.info('received webcam image', webcamImage);
      this.webcamImage = webcamImage;
     }
    
     public get triggerObservable(): Observable<void> {
      return this.trigger.asObservable();
     } 
 

 appointmentDateDetection(event: any) {

  console.log('changed date for appointment');
 }

 visitDateDetection(event: any) {
  console.log('changed date for appointment');

 }


  onDOBDetection(event: any) {

    const todayDate = new Date();

  

    const value = new Date(event.value);
   
    this.patDOB = value; // passing the DOB to the parameter above

    this.patAge = todayDate.getFullYear() - value.getFullYear(); // passing the age to the parameter above



  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
  
     reader.readAsDataURL(event.target.files[0]); // read file as data url
  
      reader.onload = (event) => { // called once readAsDataURL is completed
  
        this.lastFile = this.files.name;
        this.patPicture = reader.result;
  
      };
    }
  }

  onClickFileInputButton(): void {
    this.lastFile.nativeElement.click();
  }
  
     // HANDLING THE IMAGE TO UPLOAD
     handleFileInput(files: FileList) {
      this.files = files.item(0);
  
  
  }

  // creating the patient details below
  savePatDetailsOne() {

    console.log(this.patPicture)
    console.log(this.webcamImage)

    const patPhoneNumber = this.phonePatForm.get('phone').value;
    const kinPhoneNumber = this.phoneKinForm.get('phone').value;

    if (this.webcamImage !== null && this.patPicture === undefined) {

      if (this.patEmail === '' && this.kinEmail === '') {



        if (patPhoneNumber !== null && kinPhoneNumber === null) {

          
  
  
            const formData: FormData = new FormData();
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patPic', this.webcamImage.imageAsDataUrl);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinEmail', this.kinEmail);
          formData.append('patPhone', JSON.stringify(patPhoneNumber));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinEmail', this.kinEmail);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinEmail', this.kinEmail);
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        }
  
      }  else if (this.patEmail !== '' && this.kinEmail === '') {
        
  
  
        if (this.validateEmail(this.patEmail)) {
          if (patPhoneNumber !== null && kinPhoneNumber === null) {
  
            const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinEmail', this.kinEmail);
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        }
  
  
          
        } else  {
  
  
            this.snackBar.open('Patient Email Format is wrong', 'Close', {
              duration: 3000,
              panelClass: ['red-snackbar']
            });
        }
      } else if (this.patEmail === '' && this.kinEmail !== '') {
  
        if (this.validateEmail(this.kinEmail) === true) {
  
          if (patPhoneNumber !== null && kinPhoneNumber === null) {
  
            const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        }
  
  
        } else if (this.validateEmail(this.kinEmail) === false) {
  
          this.snackBar.open('Kin Email Format is wrong', 'Close', {
            duration: 3000,
            panelClass: ['red-snackbar']
          });
  
        }
      } else if (this.patEmail !== '' && this.kinEmail !== '') {
  
        if (this.validateEmail(this.kinEmail) === true && this.validateEmail(this.patEmail) === true) {
          if (patPhoneNumber !== null && kinPhoneNumber === null) {
  
            const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('patAddress', this.patAddress);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinEmail', this.kinEmail);
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             
  
          const formData: FormData = new FormData();
         // formData.append('manual', this.files, this.files.name);
          formData.append('patientID',  this.patientID);
          formData.append('institution', this.cookieService.get('InstitutionID'));
          formData.append('departmentGo', this.patientDepartmentSelect);
          formData.append('patBloodGroup', this.selectedBloodGroup);
          formData.append('maritalStatus', this.marit);
          formData.append('patFname', this.patFname);
          formData.append('patSurname', this.patSurname);
          formData.append('patMiddleName', this.patMiddleName);
          formData.append('patDOB', this.patDOB);
          formData.append('patAge', this.patAge);
          formData.append('patAllergies', this.patAllergies);
          formData.append('patRemarks', this.patRemarks);
          formData.append('kinFname', this.kinFname);
          formData.append('kinSurname', this.kinSurname);
          formData.append('kinRelationship', this.kinRelationship);
          formData.append('kinPhone', JSON.stringify(this.phoneKinForm.get('phone').value));
          formData.append('patPhone', JSON.stringify(this.phonePatForm.get('phone').value));
          formData.append('patGoDepartment', this.goingDepartment);
          formData.append('kinEmail', this.kinEmail);
          formData.append('payOption', this.paymentOptions);
    
          this.patientService.addPatDetailsOne(formData).subscribe((data: any) => {
    
          });
  
        }
  
  
  
  
        } else {
  
          this.snackBar.open('Patient and Next of Kin Email Formats are wrong', 'Close', {
            duration: 3000,
            panelClass: ['red-snackbar']
          });
  
  
        }
      }


///////////////////////// end here ///////////////////////////////////////
    } else if (this.webcamImage === null && this.patPicture !== undefined) {

    }
    
    
    //////////////////// no images sent right below /////////////////////////////
    
    else if (this.webcamImage === null && this.patPicture === undefined) {

      console.log('hi there')


      if (this.patEmail === '' && this.kinEmail === '') {

        console.log('heehre')


        if (patPhoneNumber !== null && kinPhoneNumber === null) {

  
          const value = {

            patientID:  this.patientID,
            institution: this.cookieService.get('InstitutionID'),
            departmentGo: this.patientDepartmentSelect,
            patBloodGroup:  this.selectedBloodGroup,
            maritalStatus: this.marit,
            patFname: this.patFname,
           patSurname: this.patSurname,
            patMiddleName: this.patMiddleName,
            patDOB: this.patDOB,
            patAge: this.patAge,
            patAllergies: this.patAllergies,
            patRemarks: this.patRemarks,
            kinFname: this.kinFname,
            kinSurname: this.kinSurname,
            kinRelationship: this.kinRelationship,
            kinEmail: this.kinEmail,
            patPhone: JSON.stringify(patPhoneNumber),
            patGoDepartment: this.goingDepartment,
           payOption: this.paymentOptions

          };
  
        
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {

          const value = {


          patientID: this.patientID,
          institution: this.cookieService.get('InstitutionID'),
           departmentGo: this.patientDepartmentSelect,
           patBloodGroup:  this.selectedBloodGroup,
           maritalStatus: this.marit,
           patFname: this.patFname,
           patSurname: this.patSurname,
           patMiddleName: this.patMiddleName,
           patDOB: this.patDOB,
           patAge: this.patAge,
           patAllergies: this.patAllergies,
           patRemarks: this.patRemarks,
           patAddress: this.patAddress,
           kinFname: this.kinFname,
           kinSurname: this.kinSurname,
           kinRelationship: this.kinRelationship,
           kinEmail: this.kinEmail,
           kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
           patGoDepartment: this.goingDepartment,
           payOption: this.paymentOptions


          };
  
             
  
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             const value = {

            
               patientID: this.patientID,
               institution: this.cookieService.get('InstitutionID'),
               departmentGo: this.patientDepartmentSelect,
               patBloodGroup: this.selectedBloodGroup,
               maritalStatus: this.marit,
               patFname: this.patFname,
               patSurname: this.patSurname,
               patMiddleName: this.patMiddleName,
               patDOB: this.patDOB,
               patAge: this.patAge,
               patAllergies: this.patAllergies,
               patRemarks: this.patRemarks,
               patAddress: this.patAddress,
               kinFname: this.kinFname,
               kinSurname: this.kinSurname,
               kinRelationship: this.kinRelationship,
               kinEmail: this.kinEmail,
              patGoDepartment: this.goingDepartment,
               payOption: this.paymentOptions
         
             }; 
  
        
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
          const value = {

             patientID: this.patientID,
             institution: this.cookieService.get('InstitutionID'),
             departmentGo: this.patientDepartmentSelect,
             patBloodGroup: this.selectedBloodGroup,
             maritalStatus: this.marit,
             patFname: this.patFname,
             patSurname: this.patSurname,
             patMiddleName: this.patMiddleName,
             patDOB: this.patDOB,
             patAge: this.patAge,
             patAllergies: this.patAllergies,
             patRemarks: this.patRemarks,
             patAddress: this.patAddress,
             kinFname: this.kinFname,
             kinSurname: this.kinSurname,
             kinRelationship: this.kinRelationship,
             kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
             patPhone: JSON.stringify(this.phonePatForm.get('phone').value),
            patGoDepartment: this.goingDepartment,
             kinEmail: this.kinEmail,
             payOption: this.paymentOptions

          };
             
  
         
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        }
  
      }  else if (this.patEmail !== '' && this.kinEmail === '') {
        
  
  
        if (this.validateEmail(this.patEmail)) {
          if (patPhoneNumber !== null && kinPhoneNumber === null) {

            const value = {

             
          patientID: this.patientID,
          institution: this.cookieService.get('InstitutionID'),
          departmentGo: this.patientDepartmentSelect,
          patBloodGroup: this.selectedBloodGroup,
          maritalStatus: this.marit,
          patFname: this.patFname,
          patSurname: this.patSurname,
          patMiddleName: this.patMiddleName,
          patDOB: this.patDOB,
          patAge: this.patAge,
          patAllergies: this.patAllergies,
          patRemarks: this.patRemarks,
          patAddress: this.patAddress,
          kinFname: this.kinFname,
          kinSurname: this.kinSurname,
          kinRelationship: this.kinRelationship,
          patPhone: JSON.stringify(this.phonePatForm.get('phone').value),
          patGoDepartment: this.goingDepartment,
          kinEmail: this.kinEmail,
          payOption: this.paymentOptions

            };
  
            
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
          const value = {

         
             patientID: this.patientID,
             institution: this.cookieService.get('InstitutionID'),
             departmentGo: this.patientDepartmentSelect,
             patBloodGroup: this.selectedBloodGroup,
             maritalStatus: this.marit,
             patFname: this.patFname,
             patSurname: this.patSurname,
             patMiddleName: this.patMiddleName,
             patDOB: this.patDOB,
             patAge: this.patAge,
             patAllergies: this.patAllergies,
             patRemarks: this.patRemarks,
             patAddress: this.patAddress,
             kinFname: this.kinFname,
             kinSurname: this.kinSurname,
             kinRelationship: this.kinRelationship,
             kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
             patGoDepartment: this.goingDepartment,
             kinEmail: this.kinEmail,
             payOption: this.paymentOptions
       

          };
             
  
         
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  

          const value = {

            patientID:  this.patientID,
            institution: this.cookieService.get('InstitutionID'),
            departmentGo: this.patientDepartmentSelect,
            patBloodGroup: this.selectedBloodGroup,
            maritalStatus: this.marit,
            patFname: this.patFname,
            patSurname: this.patSurname,
            patMiddleName: this.patMiddleName,
            patDOB: this.patDOB,
            patAge: this.patAge,
            patAllergies: this.patAllergies,
            patRemarks: this.patRemarks,
            patAddress: this.patAddress,
            kinFname: this.kinFname,
            kinSurnameL: this.kinSurname,
            kinRelationship: this.kinRelationship,
            kinEmail: this.kinEmail,
            patGoDepartment: this.goingDepartment,
            payOption: this.paymentOptions,

          }
   
        
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  

          const value = {

          
             patientID:  this.patientID,
             institution: this.cookieService.get('InstitutionID'),
             departmentGo: this.patientDepartmentSelect,
             patBloodGroup:  this.selectedBloodGroup,
             maritalStatus: this.marit,
             patFname: this.patFname,
             patSurname:  this.patSurname,
             patMiddleName: this.patMiddleName,
             patDOB: this.patDOB,
             patAge: this.patAge,
             patAllergies: this.patAllergies,
             patRemarks: this.patRemarks,
             patAddress:  this.patAddress,
             kinFname: this.kinFname,
             kinSurname: this.kinSurname,
             kinRelationship: this.kinRelationship,
             kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
             patPhone: JSON.stringify(this.phonePatForm.get('phone').value),
             patGoDepartment: this.goingDepartment,
             kinEmail: this.kinEmail,
             payOption:  this.paymentOptions

          };
 
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        }
  
  
          
        } else  {
  
  
            this.snackBar.open('Patient Email Format is wrong', 'Close', {
              duration: 3000,
              panelClass: ['red-snackbar']
            });
        }
      } else if (this.patEmail === '' && this.kinEmail !== '') {
  
        if (this.validateEmail(this.kinEmail) === true) {
  
          if (patPhoneNumber !== null && kinPhoneNumber === null) {

            const value = {

            
               patientID:  this.patientID,
               institution:  this.cookieService.get('InstitutionID'),
               departmentGo:  this.patientDepartmentSelect,
               patBloodGroup: this.selectedBloodGroup,
               maritalStatus: this.marit,
               patFname: this.patFname,
               patSurname:  this.patSurname,
               patMiddleName: this.patMiddleName,
               patDOB: this.patDOB,
               patAge:  this.patAge,
               patAllergies: this.patAllergies,
               patRemarks: this.patRemarks,
               patAddress:  this.patAddress,
               kinFname:  this.kinFname,
               kinSurname: this.kinSurname,
               kinRelationship: this.kinRelationship,
               patPhone:  JSON.stringify(this.phonePatForm.get('phone').value),
               patGoDepartment:  this.goingDepartment,
               kinEmail: this.kinEmail,
               payOption: this.paymentOptions

            };
  
     
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
          const value = {

           
          patientID: this.patientID,
          institution: this.cookieService.get('InstitutionID'),
          departmentGo: this.patientDepartmentSelect,
          patBloodGroup: this.selectedBloodGroup,
          maritalStatus: this.marit,
          patFname: this.patFname,
          patSurname: this.patSurname,
          patMiddleName: this.patMiddleName,
          patDOB: this.patDOB,
          patAge: this.patAge,
          patAllergies: this.patAllergies,
          patRemarks:  this.patRemarks,
          patAddress:  this.patAddress,
          kinFname: this.kinFname,
          kinSurname:  this.kinSurname,
          kinRelationship: this.kinRelationship,
          kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
          patGoDepartment: this.goingDepartment,
          kinEmail: this.kinEmail,
          payOption: this.paymentOptions

          };

    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
             const value = {


          patientID:   this.patientID,
           institution: this.cookieService.get('InstitutionID'),
           departmentGo: this.patientDepartmentSelect,
           patBloodGroup: this.selectedBloodGroup,
           maritalStatus: this.marit,
           patFname: this.patFname,
           fpatSurname: this.patSurname,
           patMiddleName: this.patMiddleName,
           patDOB: this.patDOB,
           patAge: this.patAge,
           patAllergies: this.patAllergies,
           patRemarks: this.patRemarks,
           patAddress: this.patAddress,
           kinFname: this.kinFname,
           kinSurname: this.kinSurname,
           kinRelationship: this.kinRelationship,
           patGoDepartment:  this.goingDepartment,
           kinEmail: this.kinEmail,
           payOption: this.paymentOptions

             };
  
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             const value = {


          patientID:   this.patientID,
          institution: this.cookieService.get('InstitutionID'),
           departmentGo: this.patientDepartmentSelect,
           patBloodGroup:  this.selectedBloodGroup,
           maritalStatus:  this.marit,
           patFname: this.patFname,
          patSurname:  this.patSurname,
           patMiddleName:  this.patMiddleName,
          patDOB: this.patDOB,
           patAge: this.patAge,
           patAllergies: this.patAllergies,
           patRemarks: this.patRemarks,
           patAddress:  this.patAddress,
           kinFname: this.kinFname,
          kinSurname: this.kinSurname,
           kinRelationship: this.kinRelationship,
           kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
           patPhone:  JSON.stringify(this.phonePatForm.get('phone').value),
           patGoDepartment: this.goingDepartment,
           kinEmail: this.kinEmail,
           payOption: this.paymentOptions

             };
  
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        }
  
  
        } else if (this.validateEmail(this.kinEmail) === false) {
  
          this.snackBar.open('Kin Email Format is wrong', 'Close', {
            duration: 3000,
            panelClass: ['red-snackbar']
          });
  
        }
      } else if (this.patEmail !== '' && this.kinEmail !== '') {
  
        if (this.validateEmail(this.kinEmail) === true && this.validateEmail(this.patEmail) === true) {
          if (patPhoneNumber !== null && kinPhoneNumber === null) {

            const value = {

               patientID:  this.patientID,
               nstitution: this.cookieService.get('InstitutionID'),
               departmentGo:  this.patientDepartmentSelect,
               patBloodGroup: this.selectedBloodGroup,
               maritalStatus:  this.marit,
               patFname: this.patFname,
               patSurname: this.patSurname,
               patMiddleName: this.patMiddleName,
               patDOB: this.patDOB,
               patAge: this.patAge,
               patAllergies: this.patAllergies,
               patRemarks: this.patRemarks,
               patAddress: this.patAddress,
               kinFname: this.kinFname,
               kinSurname: this.kinSurname,
               kinRelationship: this.kinRelationship,
               patPhone: JSON.stringify(this.phonePatForm.get('phone').value),
               patGoDepartment:  this.goingDepartment,
               kinEmail: this.kinEmail,
               payOption: this.paymentOptions

            };
  
         
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
        
  
     
        } else if (patPhoneNumber === null && kinPhoneNumber !== null) {
  
             const value = {

             
               patientID: this.patientID,
               institution: this.cookieService.get('InstitutionID'),
               departmentGo:  this.patientDepartmentSelect,
               patBloodGroup: this.selectedBloodGroup,
               maritalStatus:  this.marit,
               patFname: this.patFname,
               patSurname: this.patSurname,
               patMiddleName: this.patMiddleName,
               patDOB:  this.patDOB,
               patAge:  this.patAge,
               patAllergies:  this.patAllergies,
               patRemarks:  this.patRemarks,
               patAddress:  this.patAddress,
               kinFname: this.kinFname,
               kinSurname:  this.kinSurname,
               kinRelationship: this.kinRelationship,
               kinPhone:  JSON.stringify(this.phoneKinForm.get('phone').value),
               patGoDepartment:  this.goingDepartment,
               kinEmail:  this.kinEmail,
               payOption: this.paymentOptions

             };
  
    
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber === null && kinPhoneNumber === null) {
  
          const value = {
       
             patientID:  this.patientID,
             institution: this.cookieService.get('InstitutionID'),
             departmentGo: this.patientDepartmentSelect,
             patBloodGroup:  this.selectedBloodGroup,
             maritalStatus: this.marit,
             patFname: this.patFname,
             patSurname:  this.patSurname,
             patMiddleName: this.patMiddleName,
             patDOB: this.patDOB,
             patAge:  this.patAge,
             patAllergies:  this.patAllergies,
             patRemarks:  this.patRemarks,
             patAddress: this.patAddress,
             kinFname: this.kinFname,
             kinSurname: this.kinSurname,
             kinRelationship:  this.kinRelationship,
             kinEmail: this.kinEmail,
             patGoDepartment: this.goingDepartment,
             payOption: this.paymentOptions

          }; 

          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        } else if (patPhoneNumber !== null && kinPhoneNumber !== null) {
  
             const value = {

    
               patientID: this.patientID,
               institution: this.cookieService.get('InstitutionID'),
               departmentGo:  this.patientDepartmentSelect,
               patBloodGroup:  this.selectedBloodGroup,
               maritalStatus:  this.marit,
               patFname:  this.patFname,
               patSurname:  this.patSurname,
               patMiddleName:  this.patMiddleName,
               patDOB:  this.patDOB,
               patAge: this.patAge,
               patAllergies: this.patAllergies,
               patRemarks: this.patRemarks,
               kinFname:  this.kinFname,
               kinSurname: this.kinSurname,
               kinRelationship:  this.kinRelationship,
               kinPhone: JSON.stringify(this.phoneKinForm.get('phone').value),
               patPhone: JSON.stringify(this.phonePatForm.get('phone').value),
               patGoDepartment: this.goingDepartment,
               kinEmail:  this.kinEmail,
               payOption:  this.paymentOptions

             };
  
          
    
          this.patientService.addPatDetailsOne(value).subscribe((data: any) => {
    
          });
  
        }
  
  
  
  
        } else {
  
          this.snackBar.open('Patient and Next of Kin Email Formats are wrong', 'Close', {
            duration: 3000,
            panelClass: ['red-snackbar']
          });
  
  
        }
      }

    }




    //console.log(details);
  }

  // for other patient detail below
  savePatDetailsTwo() {

  }
  
  savePatDetailsThree() {

  }

  showWebCam() {
    this.browseImage = false;
    this.activateWebCam = true;
    this.activateWebCamOnce = true;

    this.patPicture = null;

  }

  exitCamera() {
    this.browseImage = true;
    this.activateWebCam = false;
    this.activateWebCamOnce = false;
    this.retakeSnapShot = false;

    this.webcamImage = null;
  }
  // email validation below
validateEmail(email: any) {
  const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regularExpression.test(String(email).toLowerCase());
 }

 onCountrySelected($event: Country) {
  this.patientCountry = $event.name;
}

}
