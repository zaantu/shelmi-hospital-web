import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';

  showEmailError = false;
  showPasswordError = false;
  showFieldsError = false;
  showloading = false;
  showingErrors = false;

  constructor(private router: Router, private userService: UsersService, private cookieService: CookieService) { }

  ngOnInit(): void {
  }

  toLogin() {


    if (this.email === '' && this.password === '') {

      this.showFieldsError = true;

    } else if (this.email !== '' && this.password !== '') {

      this.showFieldsError = false;

      if (this.validateEmail(this.email)) {

        this.showEmailError = false;

        this.showloading = true;

        const credentials = {
          email: this.email,
          password: this.password
        };

        // go ahead and get to the request below
        this.userService.loginUser(credentials).subscribe((data: any) => {

          

 if (data.login === false) {

      this.showingErrors = true;
      this.showloading = false;

    } else if (data.login === true) {

      const organisationData = data['data'];

      const organName = JSON.parse(JSON.stringify(organisationData.organisations[0].name));
      const organID = JSON.parse(JSON.stringify(organisationData.organisations[0]._id));


      this.cookieService.set('InstitutionName', organName);
      this.cookieService.set('InstitutionID', organID);

      this.cookieService.set('shelmi-hospital-token', data.token);
      this.router.navigateByUrl('/dashboard/summary');

      this.showloading = false;
    }
        });

       } else {
         this.showEmailError = true;
       }

    }

 


  }


  // email validation below
validateEmail(email: any) {
  const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regularExpression.test(String(email).toLowerCase());
 }


}
