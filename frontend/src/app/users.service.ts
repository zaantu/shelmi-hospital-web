import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ConstantsService } from './constants.service';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url: any;


  constructor(private http: HttpClient,
    private constant: ConstantsService, private cookieService: CookieService) {
      this.url = constant.url;

     }

     private extractData(res: Response) {
      const body = res;
      return body || {};
    }
  
  
    private handleError (error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        const err = error || '';
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return observableThrowError(errMsg);
    }

     //////////// Login User below ////////////////////////
  loginUser(credentials: any) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
     return   this.http.post(this.url + '/login', JSON.stringify(credentials), {headers: headers});

  }
}
