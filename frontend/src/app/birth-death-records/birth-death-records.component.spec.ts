import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirthDeathRecordsComponent } from './birth-death-records.component';

describe('BirthDeathRecordsComponent', () => {
  let component: BirthDeathRecordsComponent;
  let fixture: ComponentFixture<BirthDeathRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthDeathRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthDeathRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
