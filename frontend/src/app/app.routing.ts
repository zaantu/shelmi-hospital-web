import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PatientSummaryComponent } from './patient-summary/patient-summary.component';
import { AdminComponent } from './admin/admin.component';

/*export const AppRoutes: Routes = [

  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  }
]; */


export const AppRoutes: Routes = [
  {
    path: '',
    component: LoginComponent,

  },

  {
    path: 'dashboard',
    component: FullComponent,
    children: [
      {
        path: 'full',
        redirectTo: '/dashboard',
        pathMatch: 'full'

      },

      {

        path: 'summary',
        component: DashboardComponent
      },

      {
        path: 'patients',
        component: PatientSummaryComponent
      },
      {
        path: 'admin',
        component: AdminComponent
      }
    ]
  }
]; 
