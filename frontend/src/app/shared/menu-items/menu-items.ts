import { Injectable } from '@angular/core';

export interface MenuItems {
  state: string;
  name: string;
  type: string;
  icon: string;
  children?:  Navigation[];
}

export interface Navigation extends MenuItems {
  children?: MenuItems[];
}


const MENUITEMS = [
  { state: 'summary', name: 'Dashboard', type: 'link', icon: 'fa fa-home',
children : [

] },
{ state: 'patients', type: 'link', name: 'Patients', icon: 'fa fa-wheelchair',

},

  { state: 'lists', type: 'link', name: 'Laboratory', icon: 'fa fa-flask'},
  { state: 'menu', type: 'link', name: 'Pharmacy', icon: 'fa fa-medkit'},
  { state: 'tabs', type: 'link', name: 'Radiology', icon: 'fa fa-stethoscope'},
  { state: 'lists', type: 'link', name: 'Ambulance', icon: 'fa fa-ambulance' },
  { state: 'lists', type: 'link', name: 'Blood Bank', icon: 'fa fa-tint' },
  { state: 'lists', type: 'link', name: 'SMS / Communications', icon: 'fa fa-commenting' },
  { state: 'birth-and-death-records', type: 'link', name: 'Birth and Death Records', icon: 'fa fa-users'},
 { state: 'admin', type: 'link', name: 'Administration', icon: 'fa fa-address-card'},
];

@Injectable()
export class MenuItems {

  getMenuitem() {
    return MENUITEMS;
  }


}
