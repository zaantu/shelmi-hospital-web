import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationStaffComponent } from './administration-staff.component';

describe('AdministrationStaffComponent', () => {
  let component: AdministrationStaffComponent;
  let fixture: ComponentFixture<AdministrationStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
