import { Routes, Resolve } from '@angular/router';

import { ButtonsComponent } from './buttons/buttons.component';
import { GridComponent } from './grid/grid.component';
import { ListsComponent } from './lists/lists.component';
import { MenuComponent } from './menu/menu.component';
import { TabsComponent } from './tabs/tabs.component';
import { StepperComponent } from './stepper/stepper.component';
import { ExpansionComponent } from './expansion/expansion.component';
import { ChipsComponent } from './chips/chips.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ProgressSnipperComponent } from './progress-snipper/progress-snipper.component';
import { ProgressComponent } from './progress/progress.component';
import { DialogComponent } from './dialog/dialog.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { SliderComponent } from './slider/slider.component';
import { SlideToggleComponent } from './slide-toggle/slide-toggle.component';
import { AdminDepartmentsComponent } from '../admin-departments/admin-departments.component';
import { AdministrationStaffComponent } from '../administration-staff/administration-staff.component';
import { Component } from '@angular/core';
import { PatientSummaryComponent } from '../patient-summary/patient-summary.component';
import { BirthDeathRecordsComponent } from '../birth-death-records/birth-death-records.component';
import { BirthRecordsComponent } from '../birth-records/birth-records.component';
import { DeathRecordsComponent } from '../death-records/death-records.component';
import { AdminComponent } from '../admin/admin.component';

export const MaterialRoutes: Routes = [
  {
    path: 'admin',
    component: AdminComponent
  },

  {
path: 'admin-departments',
component: AdminDepartmentsComponent
  },

  {
    path: 'patients',
    component: PatientSummaryComponent,
   

  },
  {
    path: 'administration-staff',
    component: AdministrationStaffComponent
  },
  {
    path: 'birth-and-death-records',
    component: BirthDeathRecordsComponent
  },
  {
    path: 'birth-and-death-records/birth-records',
    component: BirthRecordsComponent
  },
  {
    path: 'birth-and-death-records/death-records',
    component: DeathRecordsComponent
  },
  {
    path: 'stepper',
    component: StepperComponent
  },
  {
    path: 'expansion',
    component: ExpansionComponent
  },
  {
    path: 'chips',
    component: ChipsComponent
  },
  {
    path: 'toolbar',
    component: ToolbarComponent
  },
  {
    path: 'progress-snipper',
    component: ProgressSnipperComponent
  },
  {
    path: 'progress',
    component: ProgressComponent
  },
  {
    path: 'dialog',
    component: DialogComponent
  },
  {
    path: 'tooltip',
    component: TooltipComponent
  },
  {
    path: 'snackbar',
    component: SnackbarComponent
  },
  {
    path: 'slider',
    component: SliderComponent
  },
  {
    path: 'slide-toggle',
    component: SlideToggleComponent
  },

];
