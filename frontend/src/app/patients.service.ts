import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ConstantsService } from './constants.service';
@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  url: any;


  constructor(private http: HttpClient,
    private constant: ConstantsService, private cookieService: CookieService) {
      this.url = constant.url;

    }

    private extractData(res: Response) {
      const body = res;
      return body || {};
    }
  
  
    private handleError (error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        const err = error || '';
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return observableThrowError(errMsg);
    }

   /* getEquipment(id: any): Observable<any> {

      return this.http.get(this.url + '/getEquipment/' + id).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  } */

  addPatDetailsOne(upload: any) {

    const token = this.cookieService.get('shelmi-hospital-token');

   // let tokenPlusOrganID = token + ' ' + this.cookieService.get('InstitutionID');
  
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
    .set('Authorization', `Bearer${token}`);
  return this.http.post(this.url + '/pat-details-one', JSON.stringify(upload), {headers: headers});
  
  }
  
}
