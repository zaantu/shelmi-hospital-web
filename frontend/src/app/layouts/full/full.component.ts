import { MediaMatcher } from '@angular/cdk/layout';
import {ChangeDetectorRef, Component,OnDestroy,AfterViewInit, OnInit, Inject} from '@angular/core';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { CookieService } from 'ngx-cookie-service';
import { Title } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';

/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: []
})
export class FullComponent implements OnDestroy, AfterViewInit, OnInit {

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;


  nameOfHospital = '';
  //favIcon: HTMLLinkElement = document.getElementById('appIcon');




  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    private cookieService: CookieService,
    private titleService: Title  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.nameOfHospital = this.cookieService.get('InstitutionName');

   // const favIcon = document.getElementById('appIcon');
   // (favIcon.querySelector('#appIcon') as HTMLInputElement).value = '../../../assets/images/favicon.png';
   // this.favIcon.href = '../../../assets/images/favicon.png';


  }

  ngOnInit(): void {
    this.titleService.setTitle(this.cookieService.get('InstitutionName'));

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  ngAfterViewInit() {}
}
